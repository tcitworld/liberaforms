"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import current_app
from flask_babel import gettext as _
from liberaforms.models.user import User
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.utils.dispatcher import Dispatcher
from liberaforms.utils import password as password_utils
from liberaforms.utils import ldap
from liberaforms.utils import validators
from liberaforms.utils import sanitizers


class UserDomain():
    """

    """
    user = None
    username = None
    email = None
    password = None
    ldap_uid = None

    def __init__(self, identifier, password):
        if validators.is_valid_email(identifier):
            self.email = identifier
        else:
            self.username = identifier
        self.user = self._find_user_in_db()
        if current_app.config['ENABLE_LDAP']:
            ldap_uid = self.username if self.username else self.email
            self.ldap_uid = ldap_uid  # TODO: escape uid
        self.password = password

    def get_user(self) -> User:
        if self.user:
            return self.user
        if not self.ldap_uid:
            return None

    def create_user_from_ldap(self) -> User:
        if not self.ldap_uid:
            return None
        conn, msgs = self.ldap_bind()
        if conn:  # creds have been authenticated on the ldap server
            result, msg = self.ldap_search(conn, self.ldap_uid)
            conn.unbind()
            if msg['status'] == 0:  # not authorized
                return None
            entry: dict = result[0]
            if 'email' in entry.keys():  # create a new User
                if not self.username:
                    dn_parts = ldap.ldap3_utils.dn.parse_dn(entry['dn'])
                    username = dn_parts[0][1]
                    if username == sanitizers.sanitize_username(username):
                        self.username = username
                if self.username:
                    try:
                        user = User(username=self.username,
                                    password=self.password,
                                    email=entry['email'],
                                    validated_email=True,
                                    ldap_uuid=entry['entryUUID'])
                        user.save()
                        self.user = user
                        return self.user
                    except:
                        current_app.logger.debug("Cannot create user from LDAP entry")
                        return None
        return None

    def validate_user(self) -> bool:
        """Validate the user."""
        user = self._find_user_in_db()
        if user \
           and not user.is_ldap_user() \
           and password_utils.verify_password(self.password, user.password_hash):
            return True
        if not self.ldap_uid:
            return False
        conn, msgs = self.ldap_bind()
        if 'error' in msgs[0].keys() and msgs[0]['error'] == 'LDAPSocketOpenError':
            if user and user.is_ldap_user():  # validate the user when ldap server is unreachable
                if password_utils.verify_password(self.password, user.password_hash):
                    return True
        if conn:  # creds have been authenticated on the ldap server
            result, msg = self.ldap_search(conn, self.ldap_uid)
            conn.unbind()
            if msg['status'] == 0:  # not authorized
                return False
            if user and not user.is_ldap_user():
                err_msg = f"Login failed. Non LDAP user '{self.ldap_uid}' exists in db"
                current_app.logger.info(err_msg)
                return False
            entry: dict = result[0]
            if user and user.ldap_uuid == entry['entryUUID']:
                self._sync_user(user, {**entry, **{'password': self.password}})
            return True
        return False

    def ldap_bind(self) -> tuple:  # (conn, msg:list)
        if not self.ldap_uid and not self.password:
            conn, msg = self._anonymous_bind()
            return conn, [msg]
        if not (self.ldap_uid and self.password):
            return None, [{"status": 0, "msg": "Missing %uid or password"}]
        messages = []
        if self.username:
            for dn in current_app.config['LDAP_USER_DN_LIST']:
                user_dn = dn.replace('%uid', self.ldap_uid)
                conn, msg = ldap.bind(user_dn, self.password)
                if conn:  # creds are authenticated
                    return conn, [msg]
                messages.append(msg)
        if self.email:
            # Search for user's dn with LDAP_MAIL_ATTRIB
            conn, msg = self._anonymous_bind()
            if conn:
                results, msg = self.ldap_search(conn, self.ldap_uid)
                conn.unbind()
                if msg['status'] == 1:  # ldap authorized
                    user_dn = results[0]['dn']
                    conn, msg = ldap.bind(user_dn, self.password)
                    return conn, [msg]
            return None, [msg]
        return None, messages

    @staticmethod
    def ldap_search(conn, search_text:str) -> tuple:  # (result:list, msg:dict)
        """Return an authorized ldap user."""
        if not search_text:
            return [], {"status": 0, "msg": "Missing %uid"}
        if not conn:
            current_app.logger.debug("Null LDAP connection.")
            return [], {"status": 0, "msg": "Not bound"}
        return ldap.search(conn, search_text)

    def _anonymous_bind(self):
        if not (current_app.config['LDAP_ANONYMOUS_BIND'] or
                current_app.config['LDAP_BIND_ACCOUNT']):
            error = 'LDAP_ANONYMOUS_BIND or LDAP_BIND_ACCOUNT required'
            return None, {'status': 0, 'error': error}
        if current_app.config['LDAP_ANONYMOUS_BIND']:
            username = None
            password = None
        else:
            username = current_app.config['LDAP_BIND_ACCOUNT']
            password = current_app.config['LDAP_BIND_PASSWORD']
        return ldap.bind(username, password)

    def _find_user_in_db(self) -> User:
        if self.username:
            return User.find(username=self.username)
        if self.email:
            return User.find(email=self.email)
        return None

    def _sync_user(self, user, entry) -> None:
        """Update the local user with changes made on the LDAP server."""
        update = False
        if 'email' in entry and entry['email'] != user.email:
            user.email = entry['email']
            update = True
        hashed_password = password_utils.hash_password(entry['password'])
        if user.password_hash != hashed_password:
            user.password_hash = hashed_password
            update = True
        if update:
            user.save()

    def ldap_unbind(self, conn):
        ldap.unbind(conn)

    # Non LDAP functions

    def grant_form_answers(self, invite, notify_inviter=True) -> bool:
        if not self.user:
            return False
        form = Form.find(id=invite.granted_form['id'])
        if not form:
            return False
        if not FormUser.find(user_id=self.user.id, form_id=form.id):
            FormUser(form=form, user=self.user, is_editor=False).save()
        inviter = invite.get_inviter()
        actor = inviter.username if inviter else None
        form.add_log(_("Added read only user %(email)s", email=self.user.email), actor=actor)
        if inviter and notify_inviter:
            # notify the inviter that the grant has been accepted
            Dispatcher().send_invitation_accepted(inviter=inviter,
                                                  invited_user=self.user)
        return True
