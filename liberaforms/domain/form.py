"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from functools import wraps
from flask import current_app, g, url_for
from flask import redirect, render_template, flash
import flask_login
from flask_babel import gettext as _
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import AnswerAttachment
from liberaforms.utils import validators
from liberaforms.utils.dispatcher.dispatcher import Dispatcher



def expire_all_by_date_condition():
    """Expire all forms by date condition."""
    filters = [getattr(Form, "expired") == False,
               Form.expiry_conditions["expireDate"].astext != 'false']
    forms = Form.query.filter(*filters)
    expired_forms = 0
    for form in forms:
        if not validators.is_future_date(form.expiry_conditions["expireDate"]):
            expire_form(form)
            expired_forms += 1
    return expired_forms


def expire_form(form, expired=True):
    """Expire the form and send email notification."""
    form.expired = expired
    form.save()
    if form.expired is True:
        emails = []
        for form_user in FormUser.find_all(form_id=form.id):
            if form_user.user.enabled and form_user.notifications["expiredForm"]:
                # send emails when called from CLI. Don't send email to current_user
                if not flask_login.current_user or \
                  (flask_login.current_user.is_authenticated and
                   flask_login.current_user.id != form_user.user.id):
                    emails.append(form_user.user.email)
        if emails:
            try:
                Dispatcher().send_expired_form_notification(emails, form)
            except Exception as error:
                current_app.logger.error(error)


def expire_on_date_condition(f):
    """Route decorator."""
    @wraps(f)
    def wrap(*args, **kwargs):
        form = kwargs["form"]
        if form.expiry_conditions["expireDate"] and not form.expired:
            if not validators.is_future_date(form.expiry_conditions["expireDate"]):
                expire_form(form)
        return f(*args, **kwargs)
    return wrap


def get_unavaiable_url(form, request, edition_id):
    """Return a URL when the form should not be rendered."""

    if not (form and form.is_enabled() and form.author.is_active()):
        if flask_login.current_user.is_authenticated:
            flash(_("Cannot find that form"), 'warning')
            return redirect(url_for('form_bp.my_forms'))
        return render_template('main/page-not-found.html'), 404

    if form.edit_mode:
        return render_template('try-again-soon.html'), 200

    if form.requires_consent and not form.get_consents():
        if flask_login.current_user.is_authenticated:
            flash(_("Cannot find that form"), 'warning')
            return redirect(url_for('form_bp.my_forms'))
        return render_template('main/page-not-found.html'), 404

    if request.method == 'GET' and not edition_id \
       and form.expiry_conditions["expireDate"] and not form.expired:
        if not validators.is_future_date(form.expiry_conditions["expireDate"]):
            expire_form(form)

    if form.expired and not edition_id:
        if flask_login.current_user.is_authenticated:
            flash(_("That form has expired"), 'warning')
            return redirect(url_for('form_bp.my_forms'))
        return render_template('expired.html', form=form), 200

    if form.restricted_access and not flask_login.current_user.is_authenticated:
        return render_template('main/page-not-found.html'), 404


def save_submitted_files(form,
                         answer,
                         previous_answer: bool,
                         request_files: dict,):
    """Save the attached files submitted by public form."""

    if previous_answer:
        for attachment in answer.attachments:
            if attachment.field_name not in answer.data.keys():
                attachment.delete()

    for file_field_name in request_files.keys():
        if not form.has_field(file_field_name):
            continue
        file = request_files[file_field_name]
        if not (file.filename and  # TODO: and check size
                file.content_type and
                validators.is_valid_mimetype(file, g.site.mimetypes['mimetypes'])):
            continue
        try:
            attachment = AnswerAttachment(answer)
            saved = attachment.save_attachment(file, file_field_name)
            if saved:
                answer.update_field(file_field_name, attachment.get_link())
                if form.author.set_disk_usage_alert():
                    form.author.save()
        except Exception as error:
            current_app.logger.error(error)
            err = f"Failed to save attachment: form:{form.id}, answer:{answer.id}"
            current_app.logger.error(err)


def notify_public_form_submission(form,
                                  answer,
                                  previous_answer: bool,
                                  send_confirmation: bool):
    if send_confirmation:
        email = form.get_confirmation_email_address(answer)
        if validators.is_valid_email(email):
            try:
                Dispatcher().send_answer_confirmation(email, form, answer)
            except Exception as error:
                current_app.logger.error(error)
    if previous_answer:
        form.add_log(answer.get_history_link(link_text=_("Modified an answer")),
                     actor=_("Anonymous user"))
        emails = []
        for form_user in FormUser.find_all(form_id=form.id):
            if form_user.user.enabled and form_user.notifications["anon_answer_edition"]:
                emails.append(form_user.user.email)
        if emails:
            try:
                Dispatcher().send_edited_answer_notification(emails, form)
            except Exception as error:
                current_app.logger.error(error)
    else:
        emails = []
        for form_user in FormUser.find_all(form_id=form.id):
            if form_user.user.enabled and form_user.notifications["newAnswer"]:
                emails.append(form_user.user.email)
        if emails:
            try:
                Dispatcher().send_new_answer_notification(emails, form)
            except Exception as error:
                current_app.logger.error(error)
