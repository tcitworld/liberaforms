"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import re
from datetime import datetime, timezone
import sqlalchemy
from sqlalchemy.dialects.postgresql import JSONB, TIMESTAMP, UUID
from sqlalchemy.orm.attributes import flag_modified
from sqlalchemy.sql.expression import cast
from sqlalchemy import event, func
from flask import current_app, url_for
from liberaforms import db
from liberaforms.utils.storage.storage import Storage
from liberaforms.utils.database import CRUD
from liberaforms.utils import utils
from liberaforms.utils import tokens


#from pprint import pprint as pp

class Answer(db.Model, CRUD):
    """Answer model definition."""

    __tablename__ = "answers"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    updated = db.Column(TIMESTAMP, nullable=True)
    form_id = db.Column(db.Integer,
                        db.ForeignKey('forms.id', ondelete="CASCADE"),
                        nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    marked = db.Column(db.Boolean, default=False)
    data = db.Column(JSONB, nullable=False)
    form = db.relationship("Form", viewonly=True)
    public_id = db.Column(UUID(as_uuid=True), index=True, unique=True, nullable=True)
    #editions = db.relationship("AnswerEdition", lazy='dynamic', order_by="desc(AnswerEdition.id)")
    editions = db.relationship("AnswerEdition",
                               lazy='dynamic',
                               order_by="desc(AnswerEdition.id)",
                               cascade="all, delete, delete-orphan")
    attachments = db.relationship("AnswerAttachment",
                                  lazy='dynamic',
                                  order_by="desc(AnswerAttachment.id)",
                                  cascade="all, delete, delete-orphan")


    def __init__(self, form_id, author_id, data):
        """Create a new Answer object."""
        self.created = datetime.now(timezone.utc)
        self.form_id = form_id
        self.author_id = author_id
        self.marked = False
        self.data = data

    def __str__(self):
        """Use for debugging."""
        return utils.print_obj_values(self)

    @classmethod
    def find(cls, **kwargs):
        """Return first Answer filtered by kwargs."""
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        """Return all Answers filtered by kwargs."""
        filters = []
        order = cls.created.desc()
        if 'oldest_first' in kwargs:
            if kwargs['oldest_first']:
                order = cls.created
            kwargs.pop('oldest_first')
        if 'token' in kwargs:
            filters.append(cls.token.contains({'token': kwargs['token']}))
            kwargs.pop('token')
        for key, value in kwargs.items():
            filters.append(getattr(cls, key) == value)
        return cls.query.filter(*filters).order_by(order)

    @classmethod
    def count(cls) -> int:
        """Return total of all Answers."""
        return cls.query.count()

    def update_field(self, field_name, field_value) -> None:
        """Save new value for field."""
        self.data[field_name] = field_value
        flag_modified(self, "data")
        self.save()

    def save_previous_answer(self, user_id=None) -> None:
        created = self.updated if self.updated else self.created
        AnswerEdition(created, self, user_id).save()

    def get_edition(self, edition_id):
        return AnswerEdition.find(id=edition_id, answer_id=self.id)

    @staticmethod
    def get_file_field_url(value: str):
        """Extract url from html <a>."""
        url = re.search(r'https?:[\'"]?([^\'" >]+)', value)
        return url.group(0) if url else None

    def get_field_option_labels(self, data: dict, field_name: str) -> list:
        """Return the labels of selected multi option fields."""
        field_data = data[field_name] if field_name in data else None
        element = None
        for _element in self.form.structure:
            if 'name' in _element and _element['name'] == field_name:
                element = _element
                break
        if field_data and element and "type" in element and \
                                      (element["type"] == "checkbox-group" or
                                       element["type"] == "radio-group" or
                                       element["type"] == "select"):
            option_labels = []
            for option in element["values"]:
                if option["value"] in field_data.split(", "):
                    option_labels.append(option["label"])
            return option_labels
        return []

    def get_anon_edition_url(self) -> str:
        return url_for("public_form_bp.view",
                       slug=self.form.slug,
                       edition_id=self.public_id,
                       _external=True)

    def get_history_link(self, link_text=None):
        """Return a link to inspect and answer's edition history."""
        url = url_for("answers_bp.answer_history",
                      form_id=self.form.id,
                      answer_id=self.id,
                      _external=True)
        link_text = link_text if link_text else url
        return f"<a href='{url}'>{link_text}</a>"


class AnswerEdition(db.Model, CRUD):
    """AnswerEdition model definition."""

    __tablename__ = "answer_editions"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    answer_id = db.Column(db.Integer,
                          db.ForeignKey('answers.id', ondelete="CASCADE"),
                          nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=True)
    data = db.Column(JSONB, nullable=False)
    previous_edition_id = db.Column(db.Integer, nullable=True)
    answer = db.relationship("Answer", viewonly=True)

    def __init__(self, created, answer, user_id):
        """Create a new AnswerEdition object."""
        self.created = created
        self.answer_id = answer.id
        self.user_id = user_id
        self.data = answer.data
        db.session.flush()
        self.previous_edition_id = answer.editions[0].id if answer.editions.count() else self.id

    @classmethod
    def find(cls, **kwargs):
        """Return first object filtered by kwargs."""
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        """Return all objects filtered by kwargs."""
        return cls.query.filter_by(**kwargs)


class AnswerAttachment(db.Model, CRUD, Storage):
    """AnswerAttachment model definition."""

    __tablename__ = "attachments"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    answer_id = db.Column(db.Integer,
                          db.ForeignKey('answers.id', ondelete="CASCADE"),
                          nullable=True)
    form_id = db.Column(db.Integer, db.ForeignKey('forms.id'), nullable=False)
    #form_id = db.Column(db.Integer, db.ForeignKey('forms.id',
    #                                              ondelete="CASCADE"),
    #                                              nullable=False)
    field_name = db.Column(db.String, nullable=False)
    file_name = db.Column(db.String, nullable=False)
    storage_name = db.Column(db.String, nullable=False)
    local_filesystem = db.Column(db.Boolean, default=True)  # False = Remote
    file_size = db.Column(db.Integer, nullable=False)
    encrypted = db.Column(db.Boolean, default=False)
    form = db.relationship("Form", viewonly=True)

    def __init__(self, answer):
        """Create a new AnswerAttachment object."""
        Storage.__init__(self)
        self.created = datetime.now(timezone.utc)
        self.answer_id = answer.id
        self.form_id = answer.form.id

    def __str__(self):
        """Use for debugging."""
        return utils.print_obj_values(self)

    @classmethod
    def find(cls, **kwargs):
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        return cls.query.filter_by(**kwargs)

    @classmethod
    def calc_total_size(cls, author_id=None, form_id=None):
        query = cls.query
        if author_id:
            query = query.join(Answer).filter(Answer.author_id == author_id)
        elif form_id:
            query = query.filter(cls.form_id == form_id)
        total = query.with_entities(
                    func.sum(cls.file_size.cast(sqlalchemy.Integer))
                ).scalar()
        return total if total else 0

    @property
    def directory(self):
        return f"{current_app.config['ATTACHMENT_DIR']}/{self.form_id}"

    def save_attachment(self, file, field_name):
        self.field_name = field_name
        self.file_name = file.filename
        self.storage_name = f"{utils.gen_random_string()}.{str(self.answer_id)}"
        saved = super().save_file(file, self.storage_name, self.directory)
        if saved:
            self.save()
            return True
        current_app.logger.error(f"Did not save attachment. Answer id: {self.answer_id}")
        return False

    def delete_attachment(self):
        return super().delete_file(self.storage_name, self.directory)

    def get_url(self) -> str:
        host_url = current_app.config['BASE_URL']
        return f"{host_url}/form/{self.form_id}/attachment/{self.storage_name}"

    def get_link(self) -> str:
        return f'<a href="{self.get_url()}">{self.file_name}</a>'

    def get_attachment(self):
        file_bytes = super().get_file(self.storage_name, self.directory)
        return file_bytes, self.file_name

    def does_file_exist(self) -> bool:
        return bool(super().does_file_exist(self.directory, self.storage_name))


@event.listens_for(AnswerAttachment, "after_delete")
def delete_answer_attachment(mapper, connection, target):
    deleted = target.delete_attachment()
