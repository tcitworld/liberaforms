"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import uuid
import jwt
from datetime import datetime, timezone
from sqlalchemy.dialects.postgresql import TIMESTAMP, UUID
from flask import current_app
from liberaforms import db
from liberaforms.utils.database import CRUD


class FormAuth(db.Model, CRUD):
    """FormAuth model definition."""

    __tablename__ = "form_auths"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    form_id = db.Column(db.Integer, db.ForeignKey('forms.id', ondelete="CASCADE"), nullable=False)
    token = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    form = db.relationship("Form", viewonly=True)
    enabled = db.Column(db.Boolean, default=True, nullable=False)

    def __init__(self, form):
        """Create a new FormAuth object."""
        self.created = datetime.now(timezone.utc)
        self.form_id = form.id
        self.token = uuid.uuid4().hex

    @classmethod
    def find(cls, **kwargs):
        """Return first FormAuth filtered by kwargs."""
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        """Return all FormAuth filtered by kwargs."""
        return cls.query.filter_by(**kwargs)

    def get_authorization_header_value(self) -> str:
        return jwt.encode(payload={"token": str(self.token)},
                          key=current_app.config['SECRET_KEY'],
                          algorithm="HS256")
