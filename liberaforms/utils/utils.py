"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import datetime
import uuid
import re
from pprint import pformat
import difflib
import pytz
from flask import request
from flask import current_app, has_request_context, g
from flask_babel import ngettext
import flask_login
from liberaforms.utils import password as password_utils
from liberaforms.utils import i18n


def print_obj_values(obj):
    """Print vars and values of an object."""
    values = {}
    obj_vars = vars(obj)
    for var in obj_vars:
        values[var] = obj_vars[var]
    return pformat({obj.__class__.__name__: values})


def populate_flask_g() -> None:
    """Load request context with global variables."""
    def get_highlighted_nav_item():
        #  TODO: if request is json return None
        if request.path == '/forms':
            return '/forms'
        if request.path.startswith('/form/new'):
            return '/form/new'
        if request.path.startswith('/form/'):
            return '/forms'
        if request.path.startswith('/forms/templates'):
            return '/form/new'
        if "/answers/" in request.path:
            return '/answers'
        if request.path.startswith('/user/media'):
            return '/media'
        if request.path.startswith('/user/data-consent'):
            return '/user/data-consent'
        if request.path.startswith('/user/statistics'):
            return '/user/statistics'
        if request.path.startswith('/admin/form'):
            return '/admin/forms'
        if request.path.startswith('/admin/user'):
            return '/admin/users'
        if request.path.startswith('/admin/invites'):
            return '/admin/users'
        if request.path == '/site/stats':
            return '/site/stats'
        if request.path.startswith('/server/logs'):
            return '/server/logs'
        if request.path == '/admin':
            return '/admin'
        if request.path.startswith('/site/'):
            return '/admin'
        return None
    from liberaforms.models.site import Site
    g.is_admin = False
    g.is_editor = False
    g.embedded = False
    g.site = Site.find()
    g.timezone = pytz.timezone(current_app.config['DEFAULT_TIMEZONE'])
    if flask_login.current_user.is_authenticated:
        if flask_login.current_user.enabled:
            g.is_editor = flask_login.current_user.is_editor()
            g.is_admin = flask_login.current_user.is_admin()
        g.timezone = pytz.timezone(flask_login.current_user.get_timezone())
    g.language = i18n.get_interface_locale()
    g.highlighted_nav_item = get_highlighted_nav_item()


def utc_to_g_timezone(utc_datetime):
    """Timezone adjust a datetime."""
    return utc_datetime.astimezone(g.timezone)


def stringify_date_time(date_time, utc=True, with_time=True):
    frmt = "%Y-%m-%d %H:%M:%S" if with_time else "%Y-%m-%d"
    if utc:
        return utc_to_g_timezone(date_time).strftime(frmt)
    return date_time.strftime(frmt)


def nl2br(text: str) -> str:
    return re.sub('\r?\n', "<br />", text)

# ####### Other ########


def gen_random_string():
    return gen_random_uuid()


def gen_random_uuid():
    return uuid.uuid4().hex


def str2bool(v):
    """Convert a string to a bool type."""
    return v.lower() in ("true", "1", "yes")


def human_readable_bytes(bytes_cnt: int) -> str:
    """Convert bytes into human readable string.

    1 KibiByte == 1024 Bytes
    1 Mebibyte == 1024*1024 Bytes
    1 GibiByte == 1024*1024*1024 Bytes
    """
    if bytes_cnt < 1024:
        if has_request_context():
            return ngettext("One byte", "%(number)s bytes", bytes_cnt, number=bytes_cnt)
        # when called from our CLI
        # flask_babel.gettext throws "Working outside of request context" error
        return f"{bytes_cnt} bytes"
    if bytes_cnt < 1024*1024:
        bytes_str = f"{float(round(bytes_cnt/(1024), 2))}"
        if bytes_str.endswith(".0"):
            bytes_str = bytes_str[:-2]
        return f"{bytes_str} KB"
    if bytes_cnt < 1024*1024*1024:
        bytes_str = f"{float(round(bytes_cnt/(1024*1024), 2))}"
        if bytes_str.endswith(".0"):
            bytes_str = bytes_str[:-2]
        return f"{bytes_str} MB"
    bytes_str = f"{float(round(bytes_cnt/(1024*1024*1024), 2))}"
    if bytes_str.endswith(".0"):
        bytes_str = bytes_str[:-2]
    return f"{bytes_str} GB"


def build_link(text, url, **kwargs):
    """Replaces substring between two sets of '$$' with an html link."""

    def external_link_icon():
        if "external_page" in kwargs:
            return '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-up-right"><line x1="7" y1="17" x2="17" y2="7"></line><polyline points="7 7 17 7 17 17"></polyline></svg>'
        return ""

    try:
        regex = re.compile('{}(.*?){}'.format(re.escape("$$"), re.escape("$$")))
        link_string = regex.findall(text)[0]
    except Exception as error:
        current_app.logger.warning(f"APP - build_link {error}")
        return text
    if g.embedded or "external_page" in kwargs:
        kwargs["target"] = "_blank"
    attribs = []
    for key, value in kwargs.items():
        if key.startswith("_"):
            key = key[1:]
        attribs.append(f"{key}='{value}'")

    link = "<a href='{url}' {attrs}>{link_string}{external_link}</a>".format(
                            url=url,
                            attrs=' '.join(attribs),
                            link_string=link_string,
                            external_link=external_link_icon())
    return text.replace(f"$${link_string}$$", link)


def string_to_bytes(bytes_string) -> int:
    """Convert a string to an integer.

    String format: eg. "123 GB"
    """
    try:
        size = float(bytes_string[:-3])
    except:
        return 0
    unit = bytes_string[-2:]
    if unit == 'KB':
        return size * 1024
    if unit == 'MB':
        return size * 1024*1024
    if unit == 'GB':
        return size * 1024*1024*1024
    return 0


def get_fuzzy_duration(start_time) -> str:
    """Return human friendly time difference between start_time and now."""
    now = datetime.datetime.now(datetime.timezone.utc)
    start_time = datetime.datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S.%f%z")
    duration = now - start_time
    days, seconds = duration.days, duration.seconds
    hours = days * 24 + seconds // 3600
    minutes = (seconds % 3600) // 60
    seconds = seconds % 60
    if days > 0:
        return ngettext("one day", "%(number)s days", days, number=days)
    if hours > 0:
        return ngettext("one hour", "%(number)s hours", hours, number=hours)
    if minutes > 0:
        return ngettext("one minute", "%(number)s minutes", minutes, number=minutes)
    return ngettext("one second", "%(number)s seconds", seconds, number=seconds)


def get_diff(string_1, string_2) -> str:
    if string_2 and not string_1:
        return f"<span class='ds-diff-added'>{string_2}</span>"
    if string_1 and not string_2:
        return f"<span class='ds-diff-removed'>{string_1}</span>"
    word_re = re.compile(r'(\W)')
    string_1 = word_re.split(string_1)
    string_2 = word_re.split(string_2)
    seqm = difflib.SequenceMatcher(a=string_1, b=string_2)
    output = []
    for opcode, a0, a1, b0, b1 in seqm.get_opcodes():
        if opcode == 'equal':
            output.append(''.join(seqm.a[a0:a1]))
        elif opcode == 'insert':
            output.append(f"<span class='ds-diff-added'>{''.join(seqm.b[b0:b1])}</span>")
        elif opcode == 'delete' and not (len(seqm.a[a0:a1]) == 1 and seqm.a[a0:a1][0] == ""):
            output.append(f"<span class='ds-diff-removed'>{''.join(seqm.a[a0:a1])}</span>")
        elif opcode == 'replace':
            output.append(f"<span class='ds-diff-removed'>{''.join(seqm.a[a0:a1])}</span>")
            output.append(f"<span class='ds-diff-added'>{''.join(seqm.b[b0:b1])}</span>")
    return ' '.join(output)


def fake_a_login():
    # Prevent Timing Side Channel User Enumeration
    fake_hash = "$pbkdf2-sha256$200000$5ZyTEgJAaC0lJIQwphSidA$smHO8ewaBgzizP1x36REoLyGLPJ16mtt5YWTku2mws0"
    password_utils.verify_password(gen_random_string(), fake_hash)
