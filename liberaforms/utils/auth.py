"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from functools import wraps
from flask import current_app, request, jsonify, render_template
from flask import redirect, url_for, flash, g
from flask_babel import gettext as _
import flask_login
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer
from liberaforms.models.invite import Invite


def log_denied(path=None):
    path = path if path else request.path
    if not flask_login.current_user.is_authenticated:
        msg = f'DENY {request.remote_addr} - Anonymous user denied: {path}'
        current_app.logger.info(msg)
        return
    username = flask_login.current_user.username
    if flask_login.current_user.blocked:
        msg = f'DENY {request.remote_addr} - Blocked user "{username}" denied: {path}'
    elif not flask_login.current_user.is_validated:
        msg = f'DENY {request.remote_addr} - Unvalidated user "{username}" denied: {path}'
    else:
        msg = f'DENY {request.remote_addr} - User "{username}" denied: {path}'
    current_app.logger.info(msg)


def html_response_after_denied_auth():
    if request.path == url_for('user_bp.logout'):
        return redirect(url_for('main_bp.index'))
    if flask_login.current_user.is_authenticated:
        if flask_login.current_user.blocked:
            flask_login.logout_user()
            return redirect(url_for('main_bp.index'))
        if not flask_login.current_user.is_validated:
            return redirect(url_for('user_bp.validate_to_continue'))
        return redirect(url_for('form_bp.my_forms'))
    if g.embedded:
        return render_template('main/page-not-found.html'), 404
    return render_template('main/login-to-continue.html', next=request.path)


def anonymous_user_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_anonymous:
            return f(*args, **kwargs)
        return render_template('main/page-not-found.html'), 404
    return wrap


def authenticated_user_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated:
            return f(*args, **kwargs)
        log_denied()
        return html_response_after_denied_auth()
    return wrap


def authenticated_user_required__json(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated:
            return f(*args, **kwargs)
        log_denied()
        return jsonify("Denied"), 401
    return wrap


def unvalidated_user_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and not \
           flask_login.current_user.is_validated:
            return f(*args, **kwargs)
        log_denied()
        return html_response_after_denied_auth()
    return wrap


def enabled_user_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.is_active():
            return f(*args, **kwargs)
        log_denied()
        return html_response_after_denied_auth()
    return wrap


def enabled_user_required__json(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.is_active():
            return f(*args, **kwargs)
        log_denied()
        return jsonify("Denied"), 401
    return wrap


def enabled_editor_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.is_editor() and \
           flask_login.current_user.is_active():
            return f(*args, **kwargs)
        log_denied()
        return html_response_after_denied_auth()
    return wrap


def enabled_editor_required__json(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.is_editor() and \
           flask_login.current_user.is_active():
            return f(*args, **kwargs)
        log_denied()
        return jsonify("Denied"), 401
    return wrap


def enabled_admin_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.is_admin() and \
           flask_login.current_user.is_active():
            return f(*args, **kwargs)
        log_denied()
        return html_response_after_denied_auth()
    return wrap


def enabled_admin_required__json(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.is_admin() and \
           flask_login.current_user.is_active():
            return f(*args, **kwargs)
        log_denied()
        return jsonify("Denied"), 401
    return wrap


def rootuser_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if flask_login.current_user.is_authenticated and \
           flask_login.current_user.is_root_user():
            return f(*args, **kwargs)
        log_denied()
        return html_response_after_denied_auth()
    return wrap


def instantiate_form(allow_admin=True, allow_guest=False):
    def decorator(f):
        @wraps(f)
        def wrap(*args, **kwargs):
            form = Form.find(id=kwargs['form_id'])
            if not form:
                flash(_("Cannot find that form"), 'warning')
                return redirect(url_for('form_bp.my_forms'))
            kwargs["is_form_guest"] = False
            kwargs["is_form_editor"] = False
            kwargs["form_user"] = None
            form_user = FormUser.find(form_id=kwargs['form_id'],
                                      user_id=flask_login.current_user.id)
            if form_user and form_user.is_editor:
                kwargs["is_form_editor"] = True
                kwargs['form'] = form
                kwargs["form_user"] = form_user
            if allow_guest and form_user and not form_user.is_editor:
                kwargs["is_form_guest"] = True
                kwargs['form'] = form
                kwargs["form_user"] = form_user
            if allow_admin and flask_login.current_user.is_admin():
                kwargs['form'] = form
            if "form" not in kwargs:
                flash(_("Cannot find that form"), 'warning')
                log_denied()
                return html_response_after_denied_auth()
            return f(*args, **kwargs)
        return wrap
    return decorator


def instantiate_form__json(allow_admin=True, allow_guest=False):
    def decorator(f):
        @wraps(f)
        def wrap(*args, **kwargs):
            form = Form.find(id=kwargs['form_id'])
            if not form:
                return jsonify(_("Cannot find that form")), 404
            kwargs["is_form_guest"] = False
            kwargs["is_form_editor"] = False
            kwargs["form_user"] = None
            form_user = FormUser.find(form_id=kwargs['form_id'],
                                      user_id=flask_login.current_user.id)
            if form_user and form_user.is_editor:
                kwargs["is_form_editor"] = True
                kwargs['form'] = form
                kwargs["form_user"] = form_user
            if allow_guest and form_user and not form_user.is_editor:
                kwargs["is_form_guest"] = True
                kwargs['form'] = form
                kwargs["form_user"] = form_user
            if allow_admin and flask_login.current_user.is_admin():
                kwargs['form'] = form
            if "form" not in kwargs:
                log_denied()
                return jsonify(_("Unauthorized")), 401
            return f(*args, **kwargs)
        return wrap
    return decorator


def instantiate_answer__json(allow_guest=False):
    def decorator(f):
        @wraps(f)
        def wrap(*args, **kwargs):
            answer = Answer.find(id=kwargs['answer_id'])
            if not answer:
                return jsonify(_("Cannot find that answer")), 404
            kwargs["is_form_guest"] = False
            kwargs["is_form_editor"] = False
            form_user = FormUser.find(form_id=answer.form_id,
                                      user_id=flask_login.current_user.id)
            if form_user and form_user.is_editor:
                kwargs["is_form_editor"] = True
                kwargs['answer'] = answer
            if allow_guest and form_user and not form_user.is_editor:
                kwargs["is_form_guest"] = True
                kwargs['answer'] = answer
            if "answer" not in kwargs:
                log_denied()
                return jsonify(_("Unauthorized")), 401
            return f(*args, **kwargs)
        return wrap
    return decorator


def instantiate_invite(as_admin=False):
    def decorator(f):
        @wraps(f)
        def wrap(*args, **kwargs):

            def return_not_found():
                flash(_("Opps! We cannot find that invitation"), 'warning')
                if as_admin and flask_login.current_user.is_admin():
                    return redirect(url_for('admin_bp.list_invites'))
                return redirect(url_for('form_bp.my_forms'))

            invite = Invite.find(id=kwargs["invite_id"])
            if not invite:
                return return_not_found()
            kwargs["invite"] = None
            kwargs["form_user"] = None
            if as_admin and flask_login.current_user.is_admin():
                kwargs["invite"] = invite
                return f(*args, **kwargs)
            if invite.granted_form:
                form_user = FormUser.find(form_id=invite.granted_form["id"],
                                          user_id=flask_login.current_user.id,
                                          is_editor=True)
                if form_user:
                    kwargs["invite"] = invite
                    kwargs["form_user"] = form_user
                    return f(*args, **kwargs)
            return return_not_found()
        return wrap
    return decorator
