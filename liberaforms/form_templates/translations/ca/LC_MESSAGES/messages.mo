��    �      4              L
  �   M
  s   �
  g   O    �  O   �  �         �     .   �     �     �     �     
  +        ?  	   A  5   K  N   �  4   �  	          +     &   K  &   r     �     �     �     �     �     �     �     �     �     �     �     �  -     /   1  .   a  8   �     �  "   �     �          !     5     L     l     �     �     �     �     �     �     �     �        D     &   U  
   |     �     �  1   �  3   �  (        4     F     V  (   d     �     �  M   �       B   &     i  !     	   �  $   �  
   �     �     �  
   �  O        Q     Y  "   j     �     �     �     �     �     �     �  !   �          (     0     D     X     l  !   y  (   �     �     �     �            "   ;  -   ^     �  +   �  
   �  L   �     !     *     9     A  6   R     �     �     �  	   �     �     �     �  
   �     �     �  '   �  >        Z     s     �     �     �     �     �  "     #   9     ]  
   v  
   �  	   �     �  )   �  %   �     �  X   	  ,   b  U   �  =   �  :   #  $   ^     �  	   �     �  
   �  /   �  1   �          5     K  �  Z  �   F!  �   �!  m   u"    �"  _   �#  �   H$    ?%  �   S&  @   �&  '   $'  '   L'     t'  
   �'  +   �'     �'  	   �'  ?   �'  W   (  D   g(  	   �(     �(  *   �(  +   �(  +   )     I)     K)     Q)     U)     [)     _)     a)     c)     e)  	   {)     �)  !   �)  1   �)  3   �)  =   $*  B   b*     �*  .   �*     �*     �*     +     +     3+  $   N+      s+  #   �+  
   �+     �+  	   �+     �+     ,     ,     ,  J   %,  -   p,     �,  #   �,     �,  A   �,  '   )-  *   Q-     |-     �-     �-     �-     �-     �-  B   .     V.  G   o.     �.  #   �.     �.  -   /  
   ;/     F/     V/     c/  X   p/     �/     �/     �/     �/     0     0     0     <0     R0     Z0  #   n0     �0  
   �0     �0     �0     �0     1  &   1  &   91      `1  
   �1     �1  8   �1     �1  %   �1  =   !2     _2  5   w2     �2  <   �2     �2      3     3     3  6   +3     b3  	   h3     r3     3  
   �3     �3     �3  
   �3     �3     �3  $   �3  I   4     N4     g4  (   y4  (   �4     �4     �4  $   	5  +   .5  *   Z5     �5     �5     �5  
   �5     �5  *   �5  0   6     D6  d   R6  4   �6  V   �6  6   C7  1   z7  '   �7     �7  
   �7     �7     8  /   8  0   C8     t8     �8     �8   # Activity Feedback 
Thank you for participating in this activity. Please share with us your impression so we can improve future activities. # Congress II
Last year's congress was such a success we doing it again.

Register now for this year's sessions! # Contact Form
Have a question? Fill out this contact form and we'll get in touch as soon as possible. # Hotel booking
Come stay with us! Fill out this form to book a room at our hotel.

Hotel name
Street name, 358
City, Post code.
Country

Phone number: 123 456 789 
[Website](https://example.com)
[Map](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
 # Lottery
Win 5 tickets to the cinema! Answer this form before September 20th. # Project Application Form
Have a project? Fill out this form to tell us about your proposal. 

This form is for project applications only. If you have any other question, please use our [contact form](https://example.com/contact-form). # Restaurant booking
Do you want to taste our delicious food? Fill out this form and save the date in your agenda!

[Delicious Restaurant Map](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
Street name, 15
City, Post code, County

Phone number: 123 456 789 # Summer courses

We've prepared three days of courses and workshops.

Please reserve your place now. First in, first served. # We need your help
Please sign our petition. (check in from 12pm) (check out 12pm maximum) (from Tuesday to Sunday) (if any) (remember we only open from 20h30 to 23h30) 1 10h - 12h 10h - 13h. Computer Lab management with Free software 10h - 13h. Neutral networks. A practical presentation of our WIFI installation 10h - 13h. Presentation/demo. TPV to manage the cafe 12h - 14h 14h - 15h Lunch 16h - 19h. Social currencies. Local economy 18h - 20h. GIT for beginners Session 1 18h - 20h. GIT for beginners Session 2 2 20h30 21h 21h30 22h 3 4 5 A friend told me About right Activity feedback Add a second file if you wish. Add additional information about the project. Apply for a grant. Tell us about your proposal. Ask citizens to support your local initiative. Ask participants for their feedback about your activity. Attachments Book a room and come stay with us! Booking information Breakfast and dinner Breakfast and lunch Browsing the Internets Choose the day you want to come Comment about instructor Comment about tempos Comment about the content Comments Contact Form Content Cost justification Country Date for departure Date of arrival Do you want to taste our delicious food? Save a date in your agenda! Do you want us to send you a reminder? Double bed Double bed + single bed Email Explain here your project. Focus on what and how. Explain what the requested budget will be used for. Full board (breakfast, lunch and dinner) Future activities Have a comment? Hotel booking How did you find out about this lottery? How many people are you? How should we call you? I allow you to keep the information I submit for future funding opportunities I receive your newletters I want you to erase all information if the project is not selected I'm eating else where I'm eating out, thanks for asking ID number If you win, we'll send you an email! Instructor Just breakfast Just dinner Just lunch Let attendees choose one of two talks running in parallel and their lunch menu. Lottery Lunch and dinner Maximum 10 people per reservation. Message Name Name or nick No, thanks. I will remember. One day congress Organization Other information Other things you want to express. Poor to excellent Privacy Project Application Project description Project information Project name Put your name down to win a prize Rate the explainations of the instructor Rate the quality of content Requested amount Restaurant booking Room 1. AI descrimination Room 1. Municipal Strategies Room 2. Build an anonymous website Room 2. Decentralized tech. Freedom of speech Save our shelter Shared room (bunk beds with 3 other people) Single bed Students can enroll in a variety of activities spread out across three days. Subjects Summer courses Surname Telephone number Tell us if you are interested in a particular subject. Tempos Thursday 27th Too long Too short Tuesday 25th Type of room Vegan Vegaterian Website Wednesday 26th What did you like? What can we improve? What should we do if your project is not immediately selected? When do you want to eat? Will you eat at the hotel? Yes, by email 3 days before Yes, by email 5 days before Yes, by phone 3 days before Yes, by phone 5 days before You felt breaks were... You felt that first session was... You felt that second session was... Your contact information Your data. Your email Your name e.g. +34 123 456 789 e.g. Association, cooperative, collective e.g. Buy hardware, hire someone, etc. e.g. FediBook e.g. First session was very interesting although I did expect more information about it. e.g. GDPR compliance and linguistic justice. e.g. I did enjoy both sessions. The instructor was very passionate about the subject. e.g. I've already been in your restaurant and it's very nice! e.g. I've already stayed at your hotel and it's very nice! e.g. It was nice in general. Thanks. e.g. Just to say hello! e.g. Mary e.g. Poppins e.g. Spain e.g. There was no green tea in the coffeebreak. e.g. We want to write a book about the Fediverse. e.g. https://fedi.cat e.g. mary@example.com up to €5,000 Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: info@liberaforms.org
POT-Creation-Date: 2023-10-03 09:39+0200
PO-Revision-Date: 2023-08-23 13:11+0000
Last-Translator: Porrumentzio <porrumentzio@riseup.net>
Language: ca
Language-Team: Catalan <https://hosted.weblate.org/projects/liberaforms/form-templates-liberaforms/ca/>
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.3
 # Avaluació d'activitat
Gràcies per participar en aquesta activitat. Si us plau, comparteix amb nosaltres les teves impressions. Així podrem millorar futures activitats. # Congrés II
El congrés de l'any passat va anar tant bé que el farem un altre cop.

Registra't ara per les sessions d'enguany! # Formulari de contacte
Tens alguna pregunta? Omple aquest formulari i et contactarem el més aviat possible. # Reserva d'hotel
Queda't amb nosaltres! Omple aquest formulari per reservar una habitació.

Nom de l'hotel
Carrer, 358
Ciutat i codi postal.
País

Telèfon: 123 456 789
[Web](https://example.com)
[Mapa](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
 # Sorteig
Guanya 5 tiquets per anar al cinema! Omple aquest formulari abans del 20 de setembre. # Proposta de projecte
Tens un projecte? Omple aquest formulari per explicar-nos la teva proposta.

Aquest formulari és només per propostes. Si tens preguntes, si us plau, usa el nostre [formulari de contacte](https://example.com/contact-form). # Reserva a restaurant
Vols tastar els nostres deliciosos plats? Omple aquest formulari i desa la data a la teva agenda!

[Mapa del restaurant Deliciós](https://www.openstreetmap.org/#map=12/39.5975/3.3237)
Carrer i número
Ciutat, codi postal i país

Telèfon: 123 456 789 # Cursos d'estiu

Hem preparat tres dies de cursos i tallers.

Si us plau, reserva la teva plaça. Els registres es faran per ordre d'arribada. # Necessitem la teva ajuda
Si us plau, signa la nostra petició. (entrada a partir de les 12 del migdia) (sortida màxim fins les 12 del migdia) (de dimarts a diumenge) (si escau) (recorda que només obrim de 20h30 a 23h30) 1 10h - 12h 10h - 13h. Gestió d'aules informàtiques amb programari lliure 10h - 13h. Xarxes neutrals. Una presentació pràctica de la nostra instal·lació WIFI 10h - 13h. Presentació/demostració. TPV per gestionar la cafeteria 12h - 14h 14h - 15h Dinar 16h - 19h. Monedes socials. Economia local 18h - 20h. GIT per a principiants Sessió 1 18h - 20h. GIT per a principiants Sessió 2 2 20h30 21h 21h30 22h 3 4 5 M'ho va dir una amiga Suficient Avaluació d'activitat Si vols, adjunta un segon fitxer. Afegeix informació addicional sobre el projecte. Demana ajut econòmic. Explica'ns la teva proposta. Demana suport a la ciutadania per a la teva iniciativa local. Pregunta a les participants què els ha semblat la teva activitat. Adjunts Queda't amb nosaltres, reserva una habitació! Informació de la reserva Esmorzar i sopar Esmorzar i dinar Navegant per les Internets Tria el dia que vols venir Fes un comentari sobre el formador/a Fes un comentari sobre els temps Fes un comentari sobre el contingut Comentaris Formulari de contacte Contingut Justificació de les despeses País Dia de sortida Dia d'arribada Vols tastar els nostres deliciosos plats? Reserva un dia a la teva agenda! Vols que et fem un recordatori de la reserva? Llit de matrimoni Llit de matrimoni + llit individual Correu electrònic Explica aquí el teu projecte. Posa el focus en el què i el com. Explica en què s'usarà el pressupost. Pensió completa (esmorzar, dinar i sopar) Properes activitats Vols afegir algun comentari? Reserva d'hotel Com has trobat aquest sorteig? Quantes persones sereu? Com vols que ens adrecem a tu? Permeto que guardeu la informació donada per futures oportunitats Rebo el vostre butlletí Vull que esborreu tota la informació si el projecte no és seleccionat No em quedaré a sopar Mengem fora, gràcies per preguntar Document d'identitat (DNI) Si guanyes, t'enviarem un correu electrònic! Formador/a Només esmorzar Només sopar Només dinar Permet a les assistents escollir una o dues xerrades en paral·lel i el menú del dinar. Sorteig Dinar i sopar Màxim 10 persones per reserva. Missatge Nom Nom o pseudònim No, gràcies. Me'n recordaré. Congrés d'un sol dia Entitat Altres informacions Altres coses que vulguis expressar. De insuficient a excel·lent Privacitat Proposta de projecte Descripció del projecte Informació del projecte Nom del projecte Escriu el teu nom per guanyar un premi Puntua les explicacions del formador/a Puntua la qualitat del contingut Pressupost Reserva de restaurant Sala 1. Discriminació de la Intel·ligència Artificial Sala 1. Estratègies municipals Sala 2. Crea una pàgina web anònima Sala 2. Tecnologies descentralitzades. Llibertat d'expressió Salvem el nostre refugi Habitació compartida (lliteres, amb 3 persones més) Llit individual Proposem participar en diverses activitats durant tres dies. Temes Cursos d'estiu Cognoms Número de telèfon Diga'ns si tens interès per algun tema en particular. Temps Dijous 27 Massa llarga Massa curta Dimarts 25 Tipus d'habitació Vegà Vegetarià Pàgina web del projecte Dimecres 26 Què t'ha agradat? Què milloraries? Què hauríem de fer si el teu projecte no és seleccionat immediatament? A quina hora vols sopar? Àpats a l'hotel? Sí, per correu electrònic 3 dies abans Sí, per correu electrònic 5 dies abans Sí, per telèfon 3 dies abans Sí, per telèfon 5 dies abans Vas sentir que les pauses van ser... Vas sentir que la primera sessió va ser... Vas sentir que la segona sessió va ser... Informació de contacte Les teves dades. El teu correu electrònic El teu nom p.e. +34 123 456 789 p.e. Associació, cooperativa, col·lectiu p.e. Comprar maquinari, contractar a algú, etc. p.e. FediBook p.e. La primera sessió va ser molt interessant tot i que m'esperava més informació sobre el tema. p.e. Acompliment del RGPD i justícia lingüística. p.e. Em van agradar les dues sessions. La formadora va posar molta passió en el tema. p.e. Ja he estat al vostre restaurant i és molt maco! p.e. Ja he estat al vostre hotel i és molt maco! p.e. Va estar bé en general. Gràcies. p.e. Només vull saludar! p.e. Maria p.e. Poppins p.e. Espanya p.e. A la pausa del cafè no hi havia té verd. p.e. Volem escriure un llibre sobre la Fedivers. p.e. https://fedi.cat p.e. maria@exemple.com fins a 5000 euros 