
function editConsent(consent_id, is_copy, consent_form_id) {
  if (!is_copy && !consent_form_id) {
    {# make a copy of this user|site consent template #}
    var url = "/form/{{form.id}}/data-consent/copy/"+consent_id
  }else {
    var url = "/form/{{form.id}}/data-consent/"+consent_id+"/edit"
  }
  window.location.assign(url)
}
function removeConsent(consent_id) {
  $.ajax({
    url : "{{ url_for('consent_bp.delete_form_data_consent', form_id=form.id) }}",
    type: "POST",
    dataType: "json",
    data: {"consent_id": consent_id},
    beforeSend: function(xhr, settings) {
        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken)
        }
    },
    success: function(data, textStatus, jqXHR)
    {
      if (data.deleted) {
        $("#enabled-consent-"+consent_id).remove()
        if(! $(".single_consent").length) {
          $("#is_public_badge").removeClass("bg-success").addClass("bg-secondary")
          $("#is_public_badge").html("{{ _('Not public') }}")
        }
      }
    }
  });
}
