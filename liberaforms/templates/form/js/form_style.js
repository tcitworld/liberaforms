
/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

var image_background_src = "{{ form.style.background_img }}"
var color_initial_background = "{{ form.style.background_color }}"
var color_current_background = color_initial_background
var is_background_color_saved = false

const body_style_template = `
<style id="inserted_background_image_style">
  @media only screen and (min-width: 992px) {
    body {
      background: url("%image_url%") no-repeat; background-size: cover;
     }
  }
</style>
`

function commitBackgroundColor2DOM() {

  if (! is_background_color_saved) {
    if (color_initial_background) {
      $("body").css('background-color', color_initial_background)
    } else {
      if (image_background_src) {
        commitBackgroundImage2DOM()
      } else {
        $("body").prop("style", "")
        //$("body").attr("style","background-color:var(--lf-body-bg)");
      }
    }
  } else {
    // background color saved
    color_initial_background = color_current_background
    $("body").css('background-color', color_initial_background)
  }
}
function commitBackgroundImage2DOM() {
  let image_style = body_style_template.replace(/%image_url%/, image_background_src)
  removeBackgroundImageFromDOM()
  $(image_style).appendTo("head")
}
function removeBackgroundImageFromDOM() {
  $("style[id=inserted_background_image_style]").remove()
}


$("#background_color_selector_modal").on('show.bs.modal', function() {
  /* override .modal-backdrop color because we want to see the background color */
  $( "<style id='transparent_backdrop'>.modal-backdrop {background-color: transparent !important;}</style>" ).appendTo("head")
  /* set up */
  color_current_background = color_initial_background ? color_initial_background : "#e6e6e6"
  colorWheel.hex = color_current_background
  $("#background_html_color").val(color_current_background)
  $("body").css('background-color', color_current_background)
  /* remove background image when background color modal is active */
  removeBackgroundImageFromDOM()
});
$("#background_color_selector_modal").on('shown.bs.modal', function(event) {

});
$("#background_color_selector_modal").on('hide.bs.modal', function(event) {
  /* Modal hides because 1) user has cancelled or 2) user has saved */
  commitBackgroundColor2DOM()
  is_background_color_saved = false
});
$("#background_color_selector_modal").on('hidden.bs.modal', function() {
  /* remove override .modal-backdrop color. Used bu the other modals on this page */
  $("style[id=transparent_backdrop]").remove()
});

// triggered at common/insert-image-modal.html
$(document).on("add_background_image", function(event) {
  let image_url = $("#insert_background_image_form").find('#image_url').val().trim()
  image_background_src = image_url
  commitBackgroundImage2DOM()
  $('#insert_background_image_modal').modal('hide');
  let new_backgound_image = $("#insert_background_image_form").find('#image_url').val().trim()
  $.ajax({
    url : "{{ url_for('form_style_bp.set_background_image', form_id=form.id) }}",
    type: "POST",
    dataType: "json",
    data: { "image_url": image_url },
    beforeSend: function(xhr, settings) {
      if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken)
      }
    },
    success: function(data, textStatus, jqXHR) {
      image_background_src = data.background_image
      color_current_background = color_initial_background = data.background_color
      commitBackgroundImage2DOM()
    },
    error: function(data, textStatus, jqXHR)
    {

    }
  });
});

// Background color save

function saveBackgroundColor() {
  is_background_color_saved = true
  commitBackgroundColor2DOM()
  $('#background_color_selector_modal').modal('hide');

  $.ajax({
    url : "{{ url_for('form_style_bp.set_background_color', form_id=form.id) }}",
    type: "POST",
    dataType: "json",
    data: {
      "background_color": color_current_background
    },
    beforeSend: function(xhr, settings) {
      if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken)
      }
    },
    success: function(data, textStatus, jqXHR)
    {
      image_background_src = data.background_image
      color_current_background = color_initial_background = data.background_color
      commitBackgroundColor2DOM()
    }
  });
}

// Font color save

// AAA accessible contrast
const font_color_combos = [
  {"color_1": "#FFFFFF", "color_2": "#205e3b"},
  {"color_1": "#FFFFFF", "color_2": "#0f4880"},
  {"color_1": "#FFFFFF", "color_2": "#77448b"},
  {"color_1": "#FFFFFF", "color_2": "#9f0c0c"},
  {"color_1": "#FFFFFF", "color_2": "#863600"},
  {"color_1": "#FFFFFF", "color_2": "#4f5a65"},
];
jQuery(function($) {
  font_color_combos.forEach(function(combo) {
    var template = combo_template.replace(/%color_1%/g, combo.color_1)
                                 .replace(/%color_2%/g, combo.color_2);
    $('#combo-list').append(template);
  })
});
function selectFontColor(combo) {
  $('#font_color_selector_modal').modal('hide');
  $.ajax({
    url : "{{ url_for('form_style_bp.set_font_combo', form_id=form.id) }}",
    type: "POST",
    dataType: "json",
    data: {
      "foreground_color": $(combo).css('foregroundColor'),
      "background_color": $(combo).css('backgroundColor')
    },
    beforeSend: function(xhr, settings) {
      if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken)
      }
    },
    success: function(data, textStatus, jqXHR)
    {
      $('.ds-liberaform').css('color', data.combo.foreground);
      $('.ds-liberaform').css('background-color', data.combo.background);
      $('.marked-up').find('blockquote').css('border-left-color', data.combo.foreground);
    }
  });
}
const combo_template = `
<div onclick="selectFontColor(this)" class="ds-font-color-combination g-col-6" style="border-color: %color_2%; color: %color_2%; background-color: %color_1%">
  <div class="fs-6 p-2" style="">{{ _("Example text") }}</div>
</div>
`

// Clear all styles

function clearStyle() {

  $(".ds-liberaform").css("color", "initial");
  $(".ds-liberaform").css("background-color", "#FFFFFF");
  $('.marked-up').find('blockquote').css('border-left-color', "var(--lf-gray-300)");
  $("#header_image").prop("src", "")
  $("#header_image").hide()
  $("body").prop("style", "")
  color_current_background = color_initial_background = ""
  image_background_src = ""
  $("style[id=inserted_background_image_style]").remove()
  $.ajax({
    url : "{{ url_for('form_style_bp.clear_all', form_id=form.id) }}",
    type: "POST",
    dataType: "json",
    beforeSend: function(xhr, settings) {
      if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken)
      }
    },
  });
}


// Color wheel initialization

var hexInput = document.getElementById('background_html_color')
var colorWheel = new ReinventedColorWheel({
  appendTo: document.getElementById("color_wheel_container"),
  // followings are optional properties and their default values.
  // appearance
  hex: color_current_background,
  wheelDiameter: 200,
  wheelThickness: 20,
  handleDiameter: 16,
  wheelReflectsSaturation: false,
  // handler
  onChange: function (color) {
    $("body").css('background-color', color.hex)
    $("#background_html_color").val(color.hex)
    color_current_background = color.hex
  },
});
$(function () {

})
$("#background_html_color").bind("change", function() {
   colorWheel.hex = $(this).val();
   color_current_background = $(this).val();
});


// CSS hooks

$.cssHooks.backgroundColor = {
  get: function(elem) {
    if (elem.currentStyle)
      var bg = elem.currentStyle["backgroundColor"];
    else if (window.getComputedStyle)
      var bg = document.defaultView.getComputedStyle(elem, null)
                                   .getPropertyValue("background-color");
    if (bg.search("rgb") == -1)
      return bg;
    else {
      bg = bg.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
      function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
      }
      return "#" + hex(bg[1]) + hex(bg[2]) + hex(bg[3]);
    }
  }
}
$.cssHooks.foregroundColor = {
  get: function(elem) {
    if (elem.currentStyle)
      var fg = elem.currentStyle["foregroundColor"];
    else if (window.getComputedStyle)
      var fg = document.defaultView.getComputedStyle(elem, null)
                                   .getPropertyValue("color");
    if (fg.search("rgb") == -1)
      return fg;
    else {
      fg = fg.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
      function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
      }
      return "#" + hex(fg[1]) + hex(fg[2]) + hex(fg[3]);
    }
  }
}
