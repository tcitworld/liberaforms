"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""


from flask import Blueprint, redirect, request
from flask import current_app, flash, g, url_for, jsonify
from flask_babel import gettext as _
import flask_login
from liberaforms.models.invite import Invite
from liberaforms.models.formuser import FormUser
from liberaforms.utils import auth

invite_bp = Blueprint('invite_bp', __name__)


@invite_bp.route('/invite/preview', methods=['POST'])
@auth.enabled_editor_required__json
def invite_preview():
    if request.method == 'POST' and 'language' in request.form:
        if request.form["language"] in g.site.custom_languages:
            if "text" in request.form and request.form["text"]:
                preview = Invite.get_preview(request.form["text"])
                text = Invite.prepare_message(request.form["text"], replace_vars=False)
            else:
                default_msg = Invite.default_message(request.form["language"])
                preview = Invite.get_preview(default_msg)
                text = ""
            return jsonify(text=text, preview=preview), 200
    return jsonify(preview="Not found"), 406
