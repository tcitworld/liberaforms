"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import datetime
from functools import wraps
from flask import current_app, Blueprint
from flask import g, request, render_template, redirect, url_for
from flask_babel import gettext as _
import flask_login

from liberaforms import csrf
from liberaforms.domain import form as form_domain
from liberaforms.models.form import Form
from liberaforms.models.schemas.consent import ConsentSchema
from liberaforms.models.consent import Consent
from liberaforms.models.answer import Answer, AnswerAttachment
from liberaforms.utils import utils
from liberaforms.utils import sanitizers
from liberaforms.utils import validators
from liberaforms.utils import auth
from liberaforms.utils import i18n

public_form_bp = Blueprint('public_form_bp', __name__,
                           template_folder='../templates/public_form')


def sanitized_slug_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'slug' not in kwargs:
            return render_template('main/page-not-found.html'), 404
        if kwargs['slug'] in current_app.config['RESERVED_SLUGS']:
            return render_template('main/page-not-found.html'), 404
        if len(request.args) or kwargs['slug'] != sanitizers.sanitize_slug(kwargs['slug']):
            path = request.full_path if len(request.args) else request.path
            auth.log_denied(path=path)
            return render_template('main/page-not-found.html'), 404
        return f(*args, **kwargs)
    return wrap


@public_form_bp.route('/embed/<string:slug>', methods=['GET', 'POST'])
@csrf.exempt
@sanitized_slug_required
def view_embedded_form(slug):
    flask_login.logout_user()
    g.embedded = True
    return view(slug=slug)


@public_form_bp.route('/<string:slug>', methods=['GET', 'POST'])
@public_form_bp.route('/<string:slug>/edit/<string:edition_id>', methods=['GET', 'POST'])
@sanitized_slug_required
def view(slug, edition_id=None):
    """The public form."""

    form = Form.find(slug=slug)

    unavaiable_url = form_domain.get_unavaiable_url(form, request, edition_id)
    if unavaiable_url:
        return unavaiable_url

    previous_answer = None
    if edition_id and validators.is_valid_UUID(edition_id):
        previous_answer = Answer.find(public_id=edition_id)
        if request.method == 'GET' and not previous_answer:
            return render_template('main/page-not-found.html'), 404

    if request.method == 'POST':
        submitted_data = request.form.to_dict(flat=False)
        expected_field_names = form.get_expected_field_names_on_submit(is_edition=bool(previous_answer))

        if len(submitted_data.keys()) > len(expected_field_names) + 1:  # + 1 csrf_token
            current_app.logger.info(f"FORM {request.remote_addr} - Submitted fields are greater than expected fields. Answers not saved.")
            return redirect(url_for('public_form_bp.view', slug=slug))

    consents = form.get_consents()
    validated_post = (request.method == 'POST' and Consent.validate(request.form, consents))
    if validated_post:
        # Populate the answer with the submitted data
        answer_data = {}
        for key in submitted_data:
            if key == 'csrf_token' or key not in expected_field_names:
                continue
            if key.startswith("dummy-file") and previous_answer:
                # find the existing file and populate answer_data
                field_name = submitted_data[key][0]
                attachment = AnswerAttachment.find(answer_id=previous_answer.id,
                                                   field_name=field_name)
                if attachment:
                    answer_data[field_name] = attachment.get_link()
                continue
            if key.startswith("checkbox-group"):
                value = ', '.join(submitted_data[key])  # convert list of values to a string
                key = key.rstrip('[]')  # remove tailing '[]' (appended by formbuilder)
                answer_data[key] = value
                continue
            value = sanitizers.remove_first_and_last_newlines(submitted_data[key][0].strip())
            answer_data[key] = value

        for consent in consents:
            # add consentment data to answer_data
            value = consent.field_name in submitted_data.keys()
            answer_data[consent.field_name] = value

        if previous_answer:
            answer = previous_answer
            answer.save_previous_answer()
            answer.previous_id = answer.editions[0].id
            answer.data = answer_data
            answer.updated = datetime.datetime.now(datetime.timezone.utc)
            previous_answer = answer.editions[0]
        else:
            answer = Answer(form.id, form.author_id, answer_data)
        answer.save()

    if validated_post:
        if request.files:
            # Save the attached files
            form_domain.save_submitted_files(
                form,
                answer,
                bool(previous_answer),
                request.files
            )

        if form.anon_edition and not answer.public_id:
            answer.public_id = utils.gen_random_uuid()
            answer.save()

        # Send emails and log
        form_domain.notify_public_form_submission(
            form,
            answer,
            bool(previous_answer),
            send_confirmation = form.confirmation_field_name in submitted_data
        )

        if not edition_id:
            if not form.expired and form.has_expired():
                form_domain.expire_form(form)

        after_submit_text = form.get_after_submit_text_html()
        edition_link = form.get_edit_answer_link(answer) if form.anon_edition else None
        return render_template('thankyou.html',
                               form=form,
                               after_submit_text=after_submit_text,
                               edition_link=edition_link)

    previous_answer_data = previous_answer.data if previous_answer else None
    return render_template('view.html',
                           form=form,
                           opengraph=form.get_opengraph(),
                           no_bot=True,
                           iso_lang_code=i18n.get_iso_lang_code(g.language),
                           consents=ConsentSchema(many=True).dump(consents),
                           human_readable_bytes=utils.human_readable_bytes,
                           previous_answer=previous_answer_data)
