��    
      l               �   1   �   U   �   -   E     s  5   �  k   �  T   /  t   �  x   �    r  N   �  �   �  H   Z  *   �  T   �  �   #  �   �  �   R  �   	   Attached to one form Attached to %(number)s forms Deleted '%(form_name)s' and one answer Deleted '%(form_name)s' and %(number)s answers Deleted one answer Deleted %(number)s answers One byte %(number)s bytes One pending invitation %(number)s pending invitations This form has been posted on the Fediverse once This form has been posted on the Fediverse %(number)s times You are going to delete one answer You are going to delete %(total_answers)s answers You are going to delete this form and its unique answer You are going to delete this form and its %(number)s answers Your form %(form_name)s is shared with one person. Your form %(form_name)s is shared with %(people_count)s other people. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: info@liberaforms.org
POT-Creation-Date: 2023-10-03 17:06+0200
PO-Revision-Date: 2022-04-08 07:14+0000
Last-Translator: J. Lavoie <j.lavoie@net-c.ca>
Language: pl
Language-Team: Polish <https://hosted.weblate.org/projects/liberaforms/server-liberaforms/pl/>
Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.3
 Attached to one form Attached to %(number)s forms Attached to %(number)s forms Deleted '%(form_name)s' and one answer Deleted '%(form_name)s' and %(number)s answers Deleted '%(form_name)s' and %(number)s answers Deleted one answer Deleted %(number)s answers Deleted %(number)s answers One byte %(number)s bytes %(number)s bytes One pending invitation %(number)s pending invitations %(number)s pending invitations This form has been posted on the Fediverse once This form has been posted on the Fediverse %(number)s times This form has been posted on the Fediverse %(number)s times You are going to delete one answer You are going to delete %(total_answers)s answers You are going to delete %(total_answers)s answers You are going to delete this form and its unique answer You are going to delete this form and its %(number)s answers You are going to delete this form and its %(number)s answers Your form %(form_name)s is shared with one person. Your form %(form_name)s is shared with %(people_count)s other people. Your form %(form_name)s is shared with %(people_count)s other people. 