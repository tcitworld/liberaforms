"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from ast import literal_eval
from urllib.parse import urlparse

if 'ENV_FILE' in os.environ:
    # used to run gunicorn from the command line
    from dotenv import load_dotenv
    env = os.path.join(os.getcwd(), os.environ['ENV_FILE'])
    load_dotenv(dotenv_path=env)


def get_SQLALCHEMY_DATABASE_URI():
    user = os.environ['DB_USER']
    pswd = os.environ['DB_PASSWORD']
    host = os.environ['DB_HOST']
    dbase = os.environ['DB_NAME']
    port = os.environ.get('DB_PORT', 5432)
    return f'postgresql+psycopg2://{user}:{pswd}@{host}:{port}/{dbase}'


class Config(object):
    # DEBUG = False
    TESTING = False
    FLASK_CONFIG = os.environ['FLASK_CONFIG']
    #FLASK_DEBUG = os.environ['FLASK_DEBUG']
    SESSION_COOKIE_HTTPONLY = True
    SESSION_COOKIE_SAMESITE = 'Lax'
    USE_SESSION_FOR_NEXT = True
    WTF_CSRF_ENABLED = True
    # WTF_CSRF_TIME_LIMIT. Must be less than PERMANENT_SESSION_LIFETIME
    WTF_CSRF_TIME_LIMIT = 43200  # Time to fill out a form. 12 hours
    # User sessions valid for. (refreshed on every request)
    PERMANENT_SESSION_LIFETIME = 46800  # 13h
    RESERVED_SLUGS = [
        "index", "index.html",
        "static",
        "login", "logout",
        "admin", "admins", "root",
        "profile", "user", "users",
        "form", "forms",
        "template", "templates",
        "site", "sites", "server",
        "new", "edit", "save", "update", "delete", "embed",
        "api", "metrics", "feed",
        "media", "file", "brand",
        "data-display", "data-consent",
        "answer", "answers"
    ]
    RESERVED_FORM_ELEMENT_NAMES = [
        "marked",
        "created",
        "edited",  # not used. Reserved just in case.
        "form_id"
        "csrf_token",
        "id",
        "checked",
        "send_confirmation"
    ]
    RESERVED_USERNAMES = ["system", "admin", "root", "default"]
    FORMBUILDER_DISABLED_FIELDS = ["autocomplete", "hidden", "button"]
    FORMBUILDER_DISABLED_ATTRS = ["className", "toggle", "access", "multiple"]
    FORMBUILDER_DISABLED_SUBTYPES = {'text': ['password', 'color', 'tel'],
                                     'header': ['h1', 'h3', 'h4', 'h5', 'h6']}
    FORMBUILDER_CONTROL_ORDER = ["text", "textarea", "select", "radio-group",
                                 "checkbox-group", "date", "number", "file",
                                 "header", "paragraph"]
    BABEL_TRANSLATION_DIRECTORIES = "translations;form_templates/translations"
    # http://www.lingoes.net/en/translator/langcode.htm
    LANGUAGES = {
        "es": ("Castellano", "es-ES"),
        "ca": ("Català", "ca-ES"),
        "en": ("English", "en-US"),
        "eu": ("Euskara ", "eu-ES"),
        # "eo": ("Esperanto ", "eo"),
        # "it": ("Italiano ", "it-IT"),
        # "nb": ("Norwegian Bokmål", "nb-NO")
    }
    BASE_URL = os.environ['BASE_URL']
    SERVER_NAME = urlparse(BASE_URL).netloc
    PREFERRED_URL_SCHEME = urlparse(BASE_URL).scheme
    ROOT_USER = os.environ['ROOT_USER']
    ALERT_MAILS = literal_eval(os.environ['ALERT_MAILS']) if "ALERT_MAILS" in os.environ else []
    DEFAULT_LANGUAGE = os.environ['DEFAULT_LANGUAGE']
    SECRET_KEY = os.environ['SECRET_KEY']
    SQLALCHEMY_DATABASE_URI = get_SQLALCHEMY_DATABASE_URI()
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SESSION_TYPE = os.environ['SESSION_TYPE']
    TOKEN_EXPIRATION = os.environ['TOKEN_EXPIRATION']
    if SESSION_TYPE == "memcached":
        import pylibmc as memcache
        server = os.environ['MEMCACHED_HOST']
        SESSION_MEMCACHED = memcache.Client([server])
    if 'SESSION_KEY_PREFIX' in os.environ:
        SESSION_KEY_PREFIX = os.environ['SESSION_KEY_PREFIX']
    DEFAULT_TIMEZONE = os.environ['DEFAULT_TIMEZONE']
    CRYPTO_KEY = os.environ['CRYPTO_KEY'] if 'CRYPTO_KEY' in os.environ else None
    ENABLE_UPLOADS = os.environ['ENABLE_UPLOADS'] == 'True'
    TOTAL_UPLOADS_LIMIT = os.environ['TOTAL_UPLOADS_LIMIT']
    DEFAULT_USER_UPLOADS_LIMIT = os.environ['DEFAULT_USER_UPLOADS_LIMIT']
    ENABLE_REMOTE_STORAGE = os.environ['ENABLE_REMOTE_STORAGE'] == 'True'
    MAX_MEDIA_SIZE = int(os.environ['MAX_MEDIA_SIZE'])
    MAX_ATTACHMENT_SIZE = int(os.environ['MAX_ATTACHMENT_SIZE'])
    TMP_DIR = os.environ['TMP_DIR']
    ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../'))
    ASSETS_DIR = os.path.join(ROOT_DIR, 'assets')
    UPLOADS_DIR = os.path.join(ROOT_DIR, 'uploads')
    ATTACHMENT_DIR = 'attachments'
    MEDIA_DIR = 'media'
    AVATAR_DIR = os.path.join(MEDIA_DIR, 'avatars')
    BRAND_DIR = os.path.join(MEDIA_DIR, 'brand')
    LOG_DIR = os.path.join(ROOT_DIR, os.environ['LOG_DIR'] if 'LOG_DIR' in os.environ else "logs")
    if 'FQDN' in os.environ:
        # FQDN is set when running multiple LiberaForms apps on the same server
        ATTACHMENT_DIR = os.path.join(ATTACHMENT_DIR, "hosts", os.environ['FQDN'])
        MEDIA_DIR = os.path.join(MEDIA_DIR, "hosts", os.environ['FQDN'])
        AVATAR_DIR = os.path.join(MEDIA_DIR, 'avatars')
        BRAND_DIR = os.path.join(MEDIA_DIR, 'brand')
        SESSION_KEY_PREFIX = os.environ['FQDN']
    ENABLE_PROMETHEUS_METRICS = os.environ['ENABLE_PROMETHEUS_METRICS'] == 'True'
    ENABLE_RSS_FEED = os.environ['ENABLE_RSS_FEED'] == 'True'
    ENABLE_LDAP = os.environ['ENABLE_LDAP'] == 'True'
    if ENABLE_LDAP:
        LDAP_SERVER = os.environ['LDAP_SERVER']
        LDAP_ANONYMOUS_BIND = os.environ['LDAP_ANONYMOUS_BIND'] == 'True'
        LDAP_USER_DN_LIST = literal_eval(os.environ['LDAP_USER_DN_LIST'])
        LDAP_BIND_ACCOUNT = os.environ['LDAP_BIND_ACCOUNT']
        LDAP_BIND_PASSWORD = os.environ['LDAP_BIND_PASSWORD']
        LDAP_SEARCH_BASE_DN = os.environ['LDAP_SEARCH_BASE_DN']
        LDAP_FILTER = os.environ['LDAP_FILTER']
        LDAP_MAIL_ATTRIB = os.environ['LDAP_MAIL_ATTRIB']
        LDAP_RECOVER_PASSWD_URL = os.environ['LDAP_RECOVER_PASSWD_URL']
    LOG_LEVEL = os.environ['LOG_LEVEL'] if 'LOG_LEVEL' in os.environ else "INFO"
    LOG_SERVER_PORT = int(os.environ['LOG_SERVER_PORT']) if 'LOG_SERVER_PORT' in os.environ else 9000
    DATA_DISPLAY_VERSION = "v3"


class ProductionConfig(Config):
    # DEBUG = False
    # FLASK_DEBUG = False
    SESSION_COOKIE_SECURE = True


class StagingConfig(Config):
    # FLASK_DEBUG = False
    #DEVELOPMENT = False
    SESSION_COOKIE_SECURE = True


class DevelopmentConfig(Config):
    FLASK_DEBUG = "True"
    #LOG_LEVEL = "DEBUG"
    SESSION_COOKIE_SECURE = False


class TestingConfig(Config):
    TESTING = True
    #FLASK_DEBUG = "True"
    # PRESERVE_CONTEXT_ON_EXCEPTION = False
    WTF_CSRF_ENABLED = False
    UPLOADS_DIR = os.path.join(Config.ROOT_DIR, 'tests', 'uploads')
    LOG_DIR = os.path.join(Config.ROOT_DIR, 'tests', 'logs')
    SESSION_COOKIE_SECURE = False


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'staging': StagingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
