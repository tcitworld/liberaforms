"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from factories import UserFactory
from flask import current_app
from liberaforms.models.site import Site
from liberaforms.models.user import User
from liberaforms.utils import password as password_utils
from liberaforms.utils import html_parser


def test_new_user(db):
    new_user=UserFactory()
    assert new_user.role == 'editor'
    assert new_user.validated_email is False
    assert new_user.uploads_enabled == Site.find().newuser_enableuploads


def test_new_site(app, db):
    site = Site.find()
    blurb_path = os.path.join(current_app.root_path, '../assets/blurb')
    blurb_files = os.listdir(blurb_path)
    blurb_lang_codes = [blurb_file[-5:-3] for blurb_file in blurb_files]
    assert len(blurb_files) == 5
    assert len(blurb_lang_codes) == len(blurb_files)
    for lang_code in blurb_lang_codes:
        assert lang_code in site.blurb['front_page'].keys()
        assert site.blurb['front_page'][lang_code]["markdown"]
        assert site.blurb['front_page'][lang_code]["html"]
    short_text = html_parser.get_opengraph_text(site.blurb['front_page'][site.language]['html'])
    assert len(short_text) > 0 and len(short_text) <= 155
    assert site.get_short_description() == short_text

"""
def test_factory_fixture(user_factory):
    user = user_factory(
        username = "charles-dickens",
        email = "person@example.com",
        password = "This is a secret password",
        role = 'admin',
        preferences = User.default_user_preferences(),
        blocked = False,
        admin = User.default_admin_settings(),
        validated_email = True,
        uploads_enabled = False,
        uploads_limit = "10 KB"
    )
    assert user.username == "charles-dickens"
    assert password_utils.verify_password("This is a secret password", user.password_hash) is True

def test_user_factory(user_factory):
    #Factories become fixtures automatically.
    print(type(user_factory), type(UserFactory))
    assert isinstance(user_factory, UserFactory)

"""
