"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from liberaforms.utils import validators


def test_url():
    assert validators.is_valid_url("https://example.com")
    assert validators.is_valid_url("https://www.example.com")
    assert validators.is_valid_url("https://www.example.com/page")
    assert validators.is_valid_url("https://www.example.com?param=true")
    assert not validators.is_valid_url("")
    assert not validators.is_valid_url("example.com")
    assert not validators.is_valid_url("http://liberaforms")
    assert not validators.is_valid_url("http://liberáforms.org")


def test_email():
    assert validators.is_valid_email("info@example.com")
    assert not validators.is_valid_email("info@example")
    assert not validators.is_valid_email("info@.com")
    assert not validators.is_valid_email("example.com")
    assert not validators.is_valid_email("https://example.com")
