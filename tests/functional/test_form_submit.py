"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory
from tests.utils import logout, random_slug, get_form_structure
import flask_login

class TestPublicFormSubmit():

    @classmethod
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor):
        form = FormFactory(author=editor, slug=random_slug())
        form.save()
        FormUser(user=editor, form=form, is_editor=True).save()
        self.properties['number_field_name'] = form.get_number_fields()[0]['name']
        assert self.properties['number_field_name'] == form.structure[-1:][0]['name']
        self.properties['form'] = form

    def test_auth(self, app, editor, client):
        logout(client)
        form = self.properties['form']
        assert not form.enabled
        assert form.admin_preferences['public'] is True
        assert form.edit_mode == {}
        response = client.get(
                        form.url,
                        follow_redirects=True,
                    )
        assert response.status_code == 404
        assert '<!-- page_not_found_404 -->' in response.data.decode()
        form.enabled = True
        form.admin_preferences['public'] = False
        form.save()
        response = client.get(
                        form.url,
                        follow_redirects=True,
                    )
        assert response.status_code == 404
        assert '<!-- page_not_found_404 -->' in response.data.decode()
        form.enabled = True
        form.admin_preferences['public'] = True
        form.save()
        with app.test_request_context():
            flask_login.login_user(editor)
            form.start_edit_mode()
        logout(client)
        response = client.get(
                        form.url,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- try_again_soon_page -->' in response.data.decode()
        form.enabled = True
        form.admin_preferences['public'] = True
        form.edit_mode = {}
        form.save()
        response = client.get(
                        form.url,
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- public_form_page -->' in response.data.decode()
        html = response.data.decode()
        assert '<meta name="robots" content="noindex">' in html
        assert '"label": "Name", "name": "text-1620232883208"' in html


    def test_submit_form_with_extra_field(self, client):
        form = self.properties['form']
        assert len(form.structure) == 3
        previous_answers = form.answers.count()
        response = client.post(
                        form.url,
                        data={
                            "text-1620232883208": "Julia",
                            "date-1620224710459": "A date",
                            "number-16202247163088": "2",
                            "text-1620232883209": "Not present in form structure",
                            "csrf_token": "placeholder", # TESTING does not use csrf_token
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- public_form_page -->' in response.data.decode()
        assert previous_answers == form.answers.count()


    def test_submit_form_with_invalid_field_name(self, client):
        form = self.properties['form']
        response = client.post(
                        form.url,
                        data={
                            "text-1620232883208": "Julia",
                            "text-1620232883209": "Not present in form structure",
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- thank_you_page -->" in response.data.decode()
        answer = form.answers[0]
        assert answer.data['text-1620232883208'] == "Julia"
        assert "text-1620232883209" not in answer.data


    def test_submit_form(self, client):
        form = self.properties['form']
        response = client.post(
                        form.url,
                        data={
                            "text-1620232883208": "Stella",
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- thank_you_page -->" in html
        assert form.get_after_submit_text_html() in html
        answer = form.answers[1]
        assert answer.data['text-1620232883208'] == "Stella"
        assert answer.marked is False

    def test_sumbit_embedded_form(self, client):
        form = self.properties['form']
        original_skip_emails = os.environ['SKIP_EMAILS']
        os.environ['SKIP_EMAILS'] = 'True'
        embedded_form_url = form.get_embed_url()
        response = client.get(embedded_form_url)
        assert response.status_code == 200
        assert '"label": "Name", "name": "text-1620232883208"' in response.data.decode()
        response = client.post(
                        embedded_form_url,
                        data={
                            "text-1620232883208": "Stella",
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- thank_you_page -->" in response.data.decode()
        answer = form.answers[-1]
        assert answer.data['text-1620232883208'] == "Stella"
        os.environ['SKIP_EMAILS'] = original_skip_emails


    def test_max_answers_expiration(self, client):
        original_skip_emails = os.environ['SKIP_EMAILS']
        os.environ['SKIP_EMAILS'] = 'True'
        form = self.properties['form']

        form.delete_all_answers()
        max_answers = 10
        form.expiry_conditions = {
            "fields": {},
            "totalAnswers": max_answers,
            "expireDate": None
        }
        form.save()
        assert form.expired is False

        while form.answers.count() < max_answers:
            assert form.expired is False
            response = client.post(
                            form.url,
                            data={
                                "text-1620232883208": "Julia",
                            },
                            follow_redirects=True,
                        )
        assert response.status_code == 200
        assert "<!-- thank_you_page -->" in response.data.decode()
        assert form.answers.count() == max_answers
        assert form.expired is True

        response = client.post(
                        form.url,
                        data={
                            "text-1620232883208": "Julia",
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- expired_form_page -->" in response.data.decode()
        os.environ['SKIP_EMAILS'] = original_skip_emails

    def test_max_number_field_expiration(self, client):
        """Post the form 4 times and check expiry."""
        original_skip_emails = os.environ['SKIP_EMAILS']
        os.environ['SKIP_EMAILS'] = 'True'
        form = self.properties['form']

        form.delete_all_answers()
        assert form.answers.count() == 0
        submission_cnt = 3
        number_field_value = 2
        number_field_max_total = 7
        assert submission_cnt * number_field_value == number_field_max_total - 1

        form.expiry_conditions = {
            "fields": {
                self.properties['number_field_name']: {
                    "type": "number",
                    "condition": number_field_max_total
                }
            },
            "expireDate": False,
            "totalAnswers": 0
        }
        form.save()
        assert form.expired is False
        number_field_name = self.properties['number_field_name']
        for _ in list(range(submission_cnt)):
            response = client.post(
                            form.url,
                            data={
                                "text-1620232883208": "Vicky",
                                number_field_name: number_field_value
                            },
                            follow_redirects=True,
                        )
        assert response.status_code == 200
        assert "<!-- thank_you_page -->" in response.data.decode()
        assert form.tally_number_field(number_field_name) == number_field_max_total - 1
        assert form.expired is False
        # the form should exipire after this post
        response = client.post(
                        form.url,
                        data={
                            "text-1620232883208": "Vicky",
                            number_field_name: number_field_value
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- thank_you_page -->" in response.data.decode()
        assert form.expired is True
        os.environ['SKIP_EMAILS'] = original_skip_emails

    def test_expired_form(self, client):
        form = self.properties['form']
        assert form.expired is True
        response = client.post(
                        form.url,
                        data={
                            "text-1620232883208": "Alice",
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- expired_form_page -->" in response.data.decode()
        assert form.get_expired_text_html() in response.data.decode()
