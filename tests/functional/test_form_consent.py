"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import url_for
import flask_login
from liberaforms.models.site import Site
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.consent import Consent
from liberaforms.models.formconsent import FormConsent
from tests.factories import ConsentFactory, FormFactory
from tests import user_creds
from tests import utils
from tests.utils import login, logout


class TestFormConsent():
    """Test create a form data consent
            edit consent
            delete consent
       Test attach/detach user consent.
       Test attach/detach site consent."""

    @classmethod
    def setup_class(cls):
        cls.properties = {}
        cls.site = Site.find()

    def test_requirements(self, editor):
        """Create a form that requires an attached consent."""
        form = FormFactory(author=editor, slug=utils.random_slug())
        form.requires_consent = True
        form.enabled = True
        form.save()
        assert not form.is_public()  # form requires consent
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties["form"] = form

    def test_auth(self, editor, client):
        """Test consent_bp.new_form_data_consent
                consent_bp.edit_form_data_consent
                consent_bp.delete_form_data_consent
                consent_bp.list_form_data_consents
                consent_bp.link_consent_to_form."""

        form = self.properties["form"]
        consent = ConsentFactory(user_id=editor.id)
        consent.save()

        logout(client)
        response = client.get(
                        url_for('consent_bp.new_form_data_consent', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.get(
                        url_for('consent_bp.edit_form_data_consent',
                                form_id=form.id,
                                consent_id=consent.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('consent_bp.delete_form_data_consent', form_id=form.id),
                        data={"consent_id": consent.id},
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.get(
                        url_for('consent_bp.list_form_data_consents', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('consent_bp.link_consent_to_form', form_id=form.id),
                        data={"consent_id": consent.id},
                        follow_redirects=False,
                    )
        assert response.status_code == 401

        login(client, user_creds["admin"])
        response = client.get(
                        url_for('consent_bp.new_form_data_consent', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.get(
                        url_for('consent_bp.edit_form_data_consent',
                                form_id=form.id,
                                consent_id=consent.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('consent_bp.delete_form_data_consent', form_id=form.id),
                        data={"consent_id": consent.id},
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.get(
                        url_for('consent_bp.list_form_data_consents', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('consent_bp.link_consent_to_form', form_id=form.id),
                        data={"consent_id": consent.id},
                        follow_redirects=False,
                    )
        assert response.status_code == 401

        login(client, user_creds["editor"])

    def test_new_consent(self, editor, client):
        form = self.properties["form"]
        response = client.get(
                        url_for('consent_bp.new_form_data_consent', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_edit_consent_page -->' in response.data.decode()
        assert 'add-to-library' in response.data.decode()
        initial_form_consent_cnt = Consent.find_all(form_id=form.id).count()
        initial_user_consent_cnt = Consent.find_all(user_id=editor.id).count()

        response = client.post(
                        url_for('consent_bp.new_form_data_consent', form_id=form.id),
                        data={
                            "name": "A name",
                            "language_selector": self.site.language,
                            "language": editor.preferences["language"],
                            "label": "A label",
                            "md_text": "# Hello",
                            "required": True,
                            "wizard": ""
                        },
                        follow_redirects=True

                    )
        assert response.status_code == 200
        assert '<!-- form_configuration_page -->' in response.data.decode()
        assert form.is_public()
        assert Consent.find_all(form_id=form.id).count() == initial_form_consent_cnt + 1
        assert Consent.find_all(user_id=editor.id).count() == initial_user_consent_cnt
        self.properties["consent"] = Consent.find(form_id=form.id)
        assert self.properties["consent"]

    def test_edit_consent(self, editor, client):
        form = self.properties["form"]
        consent = self.properties["consent"]
        response = client.get(
                        url_for('consent_bp.edit_form_data_consent',
                                form_id=form.id,
                                consent_id=consent.id),
                        follow_redirects=True,
                    )
        html = response.data.decode()
        assert '<!-- form_edit_consent_page -->' in html
        post_action = url_for('consent_bp.edit_form_data_consent',
                              form_id=form.id,
                              consent_id=consent.id,
                              _external=False)
        assert f'action="{post_action}"' in html

        new_name = "test edit consent name"
        response = client.post(
                        url_for('consent_bp.edit_form_data_consent',
                                form_id=form.id,
                                consent_id=consent.id),
                        data={
                            "name": new_name,
                            "language_selector": self.site.language,
                            "language": editor.preferences["language"],
                            "label": "A label",
                            "md_text": consent.text[self.site.language]["markdown"],
                            "required": consent.required,
                            "wizard": consent.wizard
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_edit_consent_page -->' in response.data.decode()
        assert consent.name == new_name

    def test_delete_consent(self, client):
        form = self.properties["form"]
        consent = self.properties["consent"]
        assert form.is_public()
        response = client.post(
                        url_for('consent_bp.delete_form_data_consent', form_id=form.id),
                        data={"consent_id": consent.id},
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.json['deleted'] is True
        assert not Consent.find(id=consent.id)
        assert not form.is_public()


    def test_attach_user_consent(self, editor, client):
        form = self.properties["form"]
        consent = ConsentFactory(user_id=editor.id)
        consent.save()
        self.properties["consent"] = consent
        assert not form.is_public()

        response = client.post(
                        url_for('consent_bp.link_consent_to_form', form_id=form.id),
                        data={"consent_id": consent.id},
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert form.is_public()
        assert form.consents.count() == 1
        assert self.properties["consent"].id == consent.id

    def test_detach_user_consent(self, editor, client):
        form = self.properties["form"]
        consent = self.properties["consent"]
        assert form.is_public()

        response = client.post(
                        url_for('consent_bp.delete_form_data_consent', form_id=form.id),
                        data={"consent_id": consent.id},
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.json['deleted'] is True
        assert Consent.find(id=consent.id, user_id=editor.id)
        assert not form.is_public()

    def test_attach_site_consent(self, client):
        form = self.properties["form"]
        consent = ConsentFactory(site_id=self.site.id)
        consent.shared = True
        consent.save()
        self.properties["consent"] = consent
        assert not form.is_public()

        response = client.post(
                        url_for('consent_bp.link_consent_to_form', form_id=form.id),
                        data={"consent_id": consent.id},
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert form.is_public()
        assert form.consents.count() == 1
        assert self.properties["consent"].id == consent.id


    def test_detach_site_consent(self, client):
        form = self.properties["form"]
        consent = self.properties["consent"]
        assert form.is_public()
        response = client.post(
                        url_for('consent_bp.delete_form_data_consent', form_id=form.id),
                        data={"consent_id": consent.id},
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.json['deleted'] is True
        assert Consent.find(id=consent.id, site_id=self.site.id)
        assert not form.is_public()

    def test_new_consent_add_to_library(self, editor, client):
        form = self.properties["form"]
        response = client.get(
                        url_for('consent_bp.new_form_data_consent', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_edit_consent_page -->' in response.data.decode()
        assert 'add-to-library' in response.data.decode()
        initial_consent_cnt = FormConsent.find_all(form_id=form.id).count()
        initial_form_consent_cnt = Consent.find_all(form_id=form.id).count()
        initial_user_consent_cnt = Consent.find_all(user_id=editor.id).count()
        response = client.post(
                        url_for('consent_bp.new_form_data_consent', form_id=form.id),
                        data={
                            "name": "A name",
                            "language_selector": self.site.language,
                            "language": editor.preferences["language"],
                            "label": "label", # {"en": "A label"},
                            "md_text": "# Hello",
                            "required": True,
                            "wizard": "",
                            "add-to-library": True
                        },
                        follow_redirects=True
                    )
        assert response.status_code == 200
        assert '<!-- form_configuration_page -->' in response.data.decode()
        assert FormConsent.find_all(form_id=form.id).count() == initial_consent_cnt + 1
        assert Consent.find_all(form_id=form.id).count() == initial_form_consent_cnt
        assert Consent.find_all(user_id=editor.id).count() == initial_user_consent_cnt + 1
