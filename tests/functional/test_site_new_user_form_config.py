"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import url_for, jsonify
from liberaforms.models.site import Site
from liberaforms.models.consent import Consent
from tests.factories import ConsentFactory
from tests import user_creds
from tests.utils import login, logout

class TestSiteNewUserFormConfig():

    @classmethod
    def setup_class(cls):
        cls.site = Site.find()
        names = ["New user consent 1", "New user consent 2"]
        cls.properties = {"names": names}

        for name in names:
            consent = ConsentFactory(name=name, site_id=cls.site.id)
            consent.save()

    def test_requirements(self, editor):
        for name in self.properties["names"]:
            assert Consent.find(name=name)

    def test_auth(self, client):
        """Test site_bp.new_user_form_preview
                site_bp.new_user_form_enable_consent"""
        logout(client)
        response = client.get(
                        url_for('site_bp.new_user_form_preview'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.new_user_form_enable_consent'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401

        login(client, user_creds['editor'])
        response = client.get(
                        url_for('site_bp.new_user_form_preview'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.new_user_form_enable_consent'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401

        login(client, user_creds['admin'])

    def test_preview_form(self, client):
        response = client.get(
                        url_for('site_bp.new_user_form_preview'),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- new_user_form_page -->' in response.data.decode()

    def test_add_consent(self, client):
        name = "New user consent 1"
        consent = Consent.find(name=name, site_id=self.site.id)
        assert consent.name == name
        assert consent.id not in self.site.registration_consent
        response = client.post(
                        url_for('site_bp.new_user_form_enable_consent'),
                        data={
                            "consent_id": consent.id,
                            "enable": True
                        },
                        follow_redirects=False,
                    )
        assert self.site.registration_consent[0] == consent.id
        assert response.status_code == 200
        assert response.is_json is True
        enabled_consents = list(response.json["enabled_consents"])
        assert len(enabled_consents) == 1
        assert len(enabled_consents) == len(self.site.registration_consent)
        assert enabled_consents[0]["id"] == consent.id

        name = "New user consent 2"
        consent = Consent.find(name=name, site_id=self.site.id)
        assert consent.name == name
        assert consent.id not in self.site.registration_consent
        response = client.post(
                        url_for('site_bp.new_user_form_enable_consent'),
                        data={
                            "consent_id": consent.id,
                            "enable": True
                        },
                        follow_redirects=False,
                    )
        assert self.site.registration_consent[1] == consent.id
        assert response.status_code == 200
        assert response.is_json is True
        enabled_consents = list(response.json["enabled_consents"])
        assert len(enabled_consents) == 2
        assert len(enabled_consents) == len(self.site.registration_consent)
        assert enabled_consents[1]["id"] == consent.id


    def test_remove_consent(self, client):
        name = "New user consent 1"
        consent = Consent.find(name=name, site_id=self.site.id)
        assert consent.name == name
        assert consent.id in self.site.registration_consent
        response = client.post(
                        url_for('site_bp.new_user_form_enable_consent'),
                        data={
                            "consent_id": consent.id,
                            "enable": False
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert consent.id not in self.site.registration_consent

    def test_delete_consent(self):
        assert len(self.site.registration_consent) == 1
        for name in self.properties["names"]:
            consent = Consent.find(name=name, site_id=self.site.id)
            consent.delete()
        assert len(self.site.registration_consent) == 0
