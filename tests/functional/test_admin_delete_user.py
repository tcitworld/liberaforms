"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
import mimetypes
from io import BytesIO
import werkzeug
from flask import url_for
from liberaforms.models.user import User
from liberaforms.models.media import Media
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer, AnswerEdition, AnswerAttachment
from tests.factories import FormFactory, UserFactory
from tests import user_creds, VALID_PASSWORD
from tests.utils import login, logout, random_slug, get_form_structure


class TestDeleteUser():
    """Create a user with: a form, answers and attachments,
       one answer edition, one media file.
       Then delete the user.
    """

    @classmethod
    def setup_class(cls):
        """Create user."""
        cls.properties = {}
        cls.user = UserFactory(username='guinea', validated_email=True)
        cls.user.save()

    def test_requirements(self, client):
        """Create required db entries."""
        logout(client)
        # create a form
        structure = get_form_structure(with_email=True, with_attachment=True)
        form = FormFactory(author=self.user,
                           slug=random_slug(),
                           structure=json.loads(structure))
        form.enabled = True
        form.anon_edition = True
        form.confirmation = {"send_email": True}
        form.save()
        FormUser(form=form, user=self.user, is_editor=True).save()
        self.properties['form'] = form
        # create answers with attachments
        valid_attachment_name = "valid_attachment.pdf"
        valid_attachment_path = f"./assets/{valid_attachment_name}"
        mimetype = mimetypes.guess_type(valid_attachment_path)[0]
        total_answers = 10
        name = "Julia"
        answer_cnt = 0
        while answer_cnt < total_answers:
            answer_cnt = answer_cnt+1
            with open(valid_attachment_path, 'rb') as attachment:
                stream = BytesIO(attachment.read())
            valid_file = werkzeug.datastructures.FileStorage(
                stream=stream,
                filename=valid_attachment_name,
                content_type=mimetype,
            )
            client.post(
                form.url,
                data={
                    "text-1620232883208": name,
                    "text-1620232903350": "anonymous@example.com",
                    "file-1622045746136": valid_file,
                    "send-confirmation": True
                },
                follow_redirects=True,
            )
        assert form.answers.count() == total_answers
        assert len(os.listdir(form.get_attachment_dir())) == total_answers
        # create an answer edition
        answer = form.answers[0]
        client.post(
            answer.get_anon_edition_url(),
            data={
                "text-1620232883208": name,
                "text-1620232903350": "anonymous@example.com",
            },
            follow_redirects=False,
        )
        assert answer.public_id is not None
        answer_edition = AnswerEdition.find(answer_id=answer.id)
        assert answer_edition is not None
        self.properties['answer_edition'] = answer_edition
        # create user media
        self.user.uploads_enabled = True
        assert self.user.can_upload()

        login(client, {'username': self.user.username, 'password': VALID_PASSWORD})
        valid_media_name = "valid_media.png"
        valid_media_path = f"./assets/{valid_media_name}"
        with open(valid_media_path, 'rb') as file:
            response = client.post(
                            url_for('media_bp.save_media'),
                            data={
                                'media_file': file,
                                'alt_text': "valid alternative text",
                            },
                            follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        # both 1. the uploaded media file and 2. the thumbnail
        assert len(os.listdir(self.user.get_media_dir())) == 2
        logout(client)

    def test_delete_user(self, client):
        """Check authorization. Then delete user."""
        response = client.get(
                        url_for('admin_bp.delete_user', user_id=self.user.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        login(client, user_creds['editor'])
        response = client.get(
                        url_for('admin_bp.delete_user', user_id=self.user.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 302
        #logout(client)
        login(client, user_creds['admin'])
        response = client.get(
                        url_for('admin_bp.delete_user', user_id=self.user.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- admin_delete_user_page -->' in response.data.decode()
        form = self.properties['form']
        response = client.post(
                        url_for('admin_bp.delete_user', user_id=self.user.id),
                        data={
                            "username": self.user.username,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- list_users_page -->' in response.data.decode()
        assert User.find(id=self.user.id) is None
        assert Media.find_all(user_id=self.user.id).count() == 0
        assert FormUser.find_all(user_id=self.user.id).count() == 0
        assert Form.find_all(author_id=self.user.id).count() == 0
        assert Answer.find_all(author_id=self.user.id).count() == 0
        assert AnswerAttachment.find_all(form_id=form.id).count() == 0
        assert AnswerEdition.find(id=self.properties['answer_edition'].id) is None

        assert os.path.isdir(self.user.get_media_dir()) is False
        assert os.path.isdir(form.get_attachment_dir()) is False
