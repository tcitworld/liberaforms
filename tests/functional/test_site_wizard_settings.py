"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import url_for
from liberaforms.models.site import Site
from tests import user_creds
from tests import utils
from tests.utils import login, logout


class TestSiteWizardSettings():

    @classmethod
    def setup_class(cls):
        cls.properties = {}
        cls.site = Site.find()

    def test_requirements(self):
        pass

    def test_auth(self, client):
        """Test site_bp.wizard_settings
                site_bp.save_organization_profile
                site_bp.save_data_protection_law
                site_bp.toggle_wizard_enforce_org
                site_bp.toggle_wizard_public_administration
                site_bp.toggle_wizard_require."""

        logout(client)
        response = client.get(
                        url_for('site_bp.wizard_settings'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.save_organization_profile'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.save_data_protection_law'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.toggle_wizard_enforce_org'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.post(
                        url_for('site_bp.toggle_wizard_public_administration'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.post(
                        url_for('site_bp.toggle_wizard_require'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401

        login(client, user_creds["editor"])
        response = client.get(
                        url_for('site_bp.wizard_settings'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.save_organization_profile'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.get(
                        url_for('site_bp.save_data_protection_law'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('site_bp.toggle_wizard_enforce_org'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.post(
                        url_for('site_bp.toggle_wizard_public_administration'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.post(
                        url_for('site_bp.toggle_wizard_require'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401

        login(client, user_creds["admin"])

    def test_save_organization_profile(self, client):
        response = client.post(
                        url_for('site_bp.save_organization_profile'),
                        data={
                            "name": "",
                            "url": "not a valid URL",
                            "tos_url": "not a valid URL",
                            "email": "not a valid email"
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- wizard_settings_page -->" in html
        assert utils.count_errors(html) == 4
        data={
            "name": "My organization",
            "url": "https://example.com/policy",
            "tos_url": "https://example.com/tos",
            "email": "info@example.com"
        }
        response = client.post(
                        url_for('site_bp.save_organization_profile'),
                        data=data,
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- wizard_settings_page -->" in html
        assert utils.count_errors(html) == 0
        data = {**data, **{"is_public_administration": False}}
        assert self.site.data_protection["organization"] == data


    def test_save_data_protection_law(self, client):

        response = client.post(
                        url_for('site_bp.save_data_protection_law'),
                        data={
                            "law": ""
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- wizard_settings_page -->" in html
        assert utils.count_errors(html) == 1

        response = client.post(
                        url_for('site_bp.save_data_protection_law'),
                        data={
                            "law": "GDPR"
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert "<!-- wizard_settings_page -->" in html
        assert utils.count_errors(html) == 0
        assert self.site.data_protection["law"] == "GDPR"


    def test_toggle_wizard_enforce_org(self, client):
        initial_value = self.site.data_protection["enforce_org"]
        response = client.post(
                        url_for('site_bp.toggle_wizard_enforce_org'),
                    )
        assert response.status_code == 200
        assert initial_value != self.site.data_protection["enforce_org"]

    def test_toggle_wizard_public_administration(self, client):
        initial_value = self.site.data_protection["organization"]["is_public_administration"]
        response = client.post(
                        url_for('site_bp.toggle_wizard_public_administration'),
                    )
        assert response.status_code == 200
        assert initial_value != self.site.data_protection["organization"]["is_public_administration"]

    def test_toggle_wizard_require(self, client):
        initial_value = self.site.data_protection["require"]
        response = client.post(
                        url_for('site_bp.toggle_wizard_require'),
                    )
        assert response.status_code == 200
        assert initial_value != self.site.data_protection["require"]
