"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import pytest


@pytest.fixture(scope='function')
def client(app):
    with app.test_client() as _client:
        yield _client
