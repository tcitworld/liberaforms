


{{ _("Publish your forms on the descentralized social media network.") }}


### {{ _("Connection name (for your convenience)") }}

{{ _("A name for this connection.") }} {{ _("It is not displayed publicly.") }}

## {{ _("Fediverse node") }}

{{ _("Enter the URL of your Fediverse node. Note that `https://` must be prepended.") }}

### {{ _("Access token") }}

{{ _("The access token allows LiberaForms to publish in your name, on your node.") }}

{{ _("Be sure you are already logged in to your account on the Fediverse. If not, do that first.") }}

{{ _("Click the `Generate` button to create an access token. You will be directed to a page on your node asking you to confirm.") }}

---

[{{ _("Connect to the fediverse") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('user_bp.fediverse_config')}})
