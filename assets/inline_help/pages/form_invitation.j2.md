

{{ _("Grant access to the form's answers.") }}

## {{ _("New user's email") }}

{{ _("The invitation will be sent to this address.") }}

## {{ _("Invitation message") }}

{{ _("The variable `[LINK]` will be converted into the invitation link and must be included in the message.") }}
