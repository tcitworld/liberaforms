
{{ _("Helpful text only visible to your users") }}.


{{ _("This information is displayed:")}}

* {{ with_link(_("On their $$profile settings$$ page"), "user.j2.md") }}
* {{ _("When they have not yet validated their email address") }}

{% if is_multilanguage_site %}
  {% include './_translation_option.j2.md' %}
{% endif %}

## {{ _("Other information") }}

{{ _("Add any information you consider important here") }}.

{% if is_multilanguage_site %}
{% include './_missing_translations.j2.md' %}
{% endif %}
---

[{{ _("Edit the information text") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('site_bp.edit_contact_info')}})
