


{{ _("Select a language from a list of available translations.") }}

{{ _("Users can change this later on their profile page.") }}

## {{ _("Can't find your language?") }}

{% include './_help_us_translate.j2.md' %}

---

[{{ _('Set new user language') }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('site_bp.set_new_user_language')}})
