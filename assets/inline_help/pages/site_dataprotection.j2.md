
{# _("LiberaForms integrates data protection policy law to help you, your users and the people who submit their data.") #}

{% set link = i18n_docs_site_url("https://docs.liberaforms.org/admin-guide/privacy-wizard/") %}
{{with_link(_("See the $$documentation website$$ for more information."), link, external_page=True)}}

## {{ _("Organization profile") }}

{{ _("The organization responsible for this installation of LiberaForms") }}.

{{ with_link(_("These values are used when you create a new $$site privacy statement$$."), "site_dataconsent_edit.j2.md") }}

+ {{ _("The name of your organization") }}: {{ _("The organization responsible for this installation of LiberaForms") }}
+ {{ _("Privacy policy web page") }}: {{ _("If possible, your Privacy policy web page") }}
+ {{ _("Terms of Service web page") }}: {{ _("If possible, your Terms of Service web page") }}
+ {{ _("Contact email") }}: {{ _("Contact email address for data protection policy enquiries") }}

{{ _("Your profile is also displayed as part of the Wizard's disclaimer.") }}

## {{ _("Share your organization profile") }}

{{ _("When set to `True` your profile is used by the wizard.") }} {{ _("Users cannot change it.")}}

## {{ _("Data protection law") }}

{{ _("The name of the law used by the wizard.") }} {{ _("Users can change it later as needed.")}}

---

[{{ _("Wizard settings") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('site_bp.wizard_settings')}})
