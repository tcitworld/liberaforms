
<div class="mb-3">
[{{ help_page(label=_("Configuration"), file_name="form.j2.md") }}]
[{{ help_page(label=_("Options"), file_name="form_options.j2.md") }}]
[{{ help_page(label=_("Answers"), file_name="form_answers.j2.md") }}]
[{{ help_page(label=_("Preview"), file_name="form_preview.j2.md") }}]
</div>
