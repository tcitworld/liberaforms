
## {{ _("Confirmation email") }}

{{ _("To enable this option include a `Short text` field in the form and change it's `type` option to `email`.") }}

{{ _("If your form includes more than one email field, LiberaForms will use the first one.") }}
