

{{ _("Use this option to display one line of text to your users when they create a new form.") }}

## {{ _("Enabled") }} / {{ _("Disabled") }}

{{ _("Enable the message for users to see it.") }}

{% if is_multilanguage_site %}
  {% include './_translation_option.j2.md' %}
{% endif %}


## {{ _("Short message") }}

{{ _("May include HTML links.") }}

{% if is_multilanguage_site %}
{% include './_missing_translations.j2.md' %}
{% endif %}
---

[{{ _("Edit the message") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('site_bp.new_form_msg')}})
