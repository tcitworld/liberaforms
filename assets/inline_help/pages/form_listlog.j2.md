

{{ _("A history of changes made to the form.") }}

{{ _("Each time an editor makes changes to the form this data is logged for your information:") }}

+ {{ _("The time") }}
+ {{ _("The editor who made the change") }}
+ {{ _("A short descriptive message") }}

{{ _("Changes made to the form's answers are also logged here.") }}
