

{{ _("Use these options to add style to the form.") }}

## {{ _("Featured image") }}

{{ with_link(_("Use one of your $$media files$$ to display an image at the top of the form."), "user_media.j2.md") }}

## {{ _("Font color") }}

{{ _("Change the color of the form's font.") }}

## {{ _("Background color") }}

{{ _("Change the background color.") }}

## {{ _("Background image") }}

{{ _("Use one of your media files to set the form's background image.") }}

## {{ _("Clear") }}

{{ _("Remove your changes and use the default form style.") }}
